### YeastCyberSecretion

This repository hosts the data to analyze the raw data and for generate figures for the manuscript "Maximizing protein production by keeping cells at optimal secretory stress levels using real-time control approaches", by Sosa-Carrillo and colleagues. 

The related paper is available on [bioRxiv](https://www.biorxiv.org/content/10.1101/2022.11.02.514931).

The repository is composed of four folders:

* code: a folder containing the core code for the experiment analysis
* analysis_figures: a folder containing the code for generating the figures
* data: an empty folder to be filled with the raw data downloaded from Zenodo at doi:10.5281/zenodo.7418639
* plasmids: a folder containing the GeneBank files of the plasmids used in this manuscript

#### Dependencies

This code makes use of the following packages:

* Pandas
* NumPy
* SciPy
* sklearn
* matplotlib
* cma

#### Instructions for Use

To run the code contained in this repository, you should perform the following steps: 

1. Download the repository and unzip the files.
2. Download the raw data from Zenodo at doi:10.5281/zenodo.7418639 and copy them in the data folder.
3. Run analysis_figures/process_raw_data_reactors.py and analysis_figures/process_raw_data_secretion.py: they will process the raw data to prepare them for the next steps.
4. Run analysis_figures/figure_1/figure_1.py or the code associated at whatever figure you are more interested in, contained in the proper folder. 

