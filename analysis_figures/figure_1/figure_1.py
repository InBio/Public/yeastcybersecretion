#!/usr/bin/env python
# coding: utf-8

# ## Import modules and select data

# In[1]:


# Python modules
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.ticker import MaxNLocator
from joypy import joyplot
import sys

# Main path
path = 'C:\\Users\\lifeware\\Desktop\\20221104_paper'

# In-house modules
sys.path.append(path + '\\code')

import getting_processed_data
import Parsing


# In[2]:


# Write the experiment date in the order that you want them

mNeon_non_sec = [20211027]

mNeon = [20220412]

experiment_dates = mNeon_non_sec + mNeon 

# Write the experiment (secretion measurement) date in the order that you want them (same as above)

mNeon_non_sec = [20211029] 

mNeon = [20220414]

sec_experiment_dates = mNeon_non_sec + mNeon 

# Secretion events threshold, below that, the secretion levels would be NaN
events_th = 10

# Order the data sets as you like
data_sets = ['mNeon-non-sec', 'mNeon']   

# if want to discard some sample, make a list of tupples for each (data set, replica, reactor), or enter ('None')

outliers = [('None')]

# Set an approximate time length of the experiment (the LED is on at time 0):
start = -3
end = 26


# In[3]:


# Here I get the data sets, tell if you use sensor strain
LED, cells_raw, cells_corrected, cells, POI, POI_median, metadata, sensor, sensor_median =         getting_processed_data.get_processed_data(experiment_dates, path, outliers)


# In[4]:


# Here I get the data sets only for secretion
sec_raw, sec = Parsing.sec_parse(experiment_dates, sec_experiment_dates, metadata, POI_median, path, events_th)

POI_median, POI_sec, induction_levels = Parsing.derived_data(sensor_median, POI_median, sec, metadata, end, path)


# In[5]:


# I am going to make a summary data frame for simplfy the data management

data_summary = metadata.copy()
        
# Here I compute steady state values of x[variable] between t_start and t_end
x = POI_median
variable = 'IF'
t_start = 20
t_end = 25
IF_ss = Parsing.steady_states(x, variable, t_start, t_end, metadata)

x = POI_median
variable = 'UPR'
UPR_ss = Parsing.steady_states(x, variable, t_start, t_end, metadata)

data_summary['induction_levels'] = list(induction_levels.values())
data_summary['IF_ss'] = IF_ss.copy()
    
POI_sec['sec'] = POI_sec['sec_raw'].copy()  

# and then use the proteomics data to scale it based on the experiment with same duty cycle

proteomics = pd.read_csv (path + '\\data\\proteomics_metric.csv')

for data_set in data_sets:
    
    sample = data_summary[data_summary['POI'] == data_set].sort_values(['DC']).iloc[-1]['sample']
    
    if np.isnan(POI_sec['sec'][POI_sec['sample'] == sample].values[0]) == True:
        
        sample = data_summary[data_summary['POI'] == data_set].sort_values(['DC']).iloc[-2]['sample']
    
    OM_factor = 10**proteomics['OM'][proteomics['POI'] == data_set].values[0]/POI_sec['sec'][POI_sec['sample'] == sample].values[0]   
    
    for sample in data_summary[data_summary['POI'] == data_set]['sample']:
        
        POI_sec['sec'].loc[POI_sec['sample'] == sample] = POI_sec['sec'].loc[POI_sec['sample'] == sample] * OM_factor
    
data_summary['sec'] = POI_sec['sec'].copy()
data_summary['UPR_ss'] = UPR_ss.copy()

# Also add induction levels to the POI_sec data frame
POI_sec['induction_levels'] = list(induction_levels.values())


# In[6]:


# Here I substract the fluorescence of secretion at induction 0, I count it as a blank 
    
group_0 = data_summary['sample'][data_summary['DC'] == 0]

for sample in group_0:

    blank = data_summary['sec'][data_summary['sample'] == sample].values[0]
    
    if np.isnan(blank) == False:
    
        replica = data_summary['replica'][data_summary['sample'] == sample].values[0]

        group = data_summary['sample'][data_summary['POI'] == data_summary['POI'][data_summary['sample'] == sample].values[0]]

        for sample_of_replica in group:

            if data_summary['replica'][data_summary['sample'] == sample_of_replica].values[0] == replica:

                data_summary['sec'].loc[data_summary['sample'] == sample_of_replica] =                 data_summary['sec'][data_summary['sample'] == sample_of_replica].copy() - blank

                POI_sec['sec'].loc[POI_sec['sample'] == sample_of_replica] =                 data_summary['sec'][data_summary['sample'] == sample_of_replica].copy()


# # Example of population dynamics

# In[7]:


params = {'legend.fontsize': 8,
          'legend.loc' :'lower right',
          'legend.markerscale'   : 1,
          'axes.labelsize' : 8,
          'xtick.labelsize' : 8,
          'ytick.labelsize' : 8,
          'axes.titlesize' : 8,

          'font.family':  'Calibri',
          'font.style':   'normal',
          'font.variant': 'normal',
          'font.weight':  100,
          'font.stretch': 'normal',
          'font.size':    8.0,
          }

plt.rcParams.update(params)


# In[8]:


# colormap
N = len(induction_levels)
cmap = plt.get_cmap('Blues', N)

# Normalizer
norm = mpl.colors.Normalize(vmin=np.min(data_summary['induction_levels']),                  vmax=np.max(data_summary['induction_levels']))

# creating ScalarMappable
sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
sm.set_array([])

samples = [9]

for sample in samples:

    colorVal = sm.to_rgba(data_summary['induction_levels'][data_summary['sample'] == sample].values[0])   

    df = POI[sample][(POI[sample]['time_norm'] > 0) & (POI[sample]['time_norm'] < 10) & (POI[sample]['IF'] > 0) & (POI[sample]['IF'] > 0) & (POI[sample]['IF'] < 0.05)]

    fig, axes = joyplot(df, by = 'time_norm', column = 'IF',                        linewidth = 0.5, overlap = 2, color = colorVal, alpha = 1)

ax = plt.gca()
ax.axes.xaxis.set_ticks([])
ax.axes.yaxis.set_ticks([])

plt.savefig('distributions' + '_' + str(sample) + '.svg')


# In[9]:


# colormap
N = len(induction_levels)
cmap = plt.get_cmap('Blues', N)

# Normalizer
norm = mpl.colors.Normalize(vmin=np.min(data_summary['induction_levels']),                  vmax=np.max(data_summary['induction_levels']))

# creating ScalarMappable
sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
sm.set_array([])

samples = [12]

for sample in samples:

    colorVal = sm.to_rgba(data_summary['induction_levels'][data_summary['sample'] == sample].values[0])   

    df = POI[sample][(POI[sample]['time_norm'] > 0) & (POI[sample]['time_norm'] < 10) & (POI[sample]['IF'] > 0) & (POI[sample]['IF'] > 0) & (POI[sample]['IF'] < 0.05)]

    fig, axes = joyplot(df, by = 'time_norm', column = 'IF',                        linewidth = 0.5, overlap = 2, color = colorVal, alpha = 1)

ax = plt.gca()
ax.axes.xaxis.set_ticks([])
ax.axes.yaxis.set_ticks([])

plt.savefig('distributions' + '_' + str(sample) + '.svg')


# In[10]:


# colormap
N = len(induction_levels)
cmap = plt.get_cmap('Blues', N)

# Normalizer
norm = mpl.colors.Normalize(vmin=np.min(data_summary['induction_levels']),                  vmax=np.max(data_summary['induction_levels']))

# creating ScalarMappable
sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
sm.set_array([])

samples = [16]

for sample in samples:

    colorVal = sm.to_rgba(data_summary['induction_levels'][data_summary['sample'] == sample].values[0])   

    df = POI[sample][(POI[sample]['time_norm'] > 0) & (POI[sample]['time_norm'] < 10) & (POI[sample]['IF'] > 0) & (POI[sample]['IF'] > 0) & (POI[sample]['IF'] < 0.05)]

    fig, axes = joyplot(df, by = 'time_norm', column = 'IF',                        linewidth = 0.5, overlap = 2, color = colorVal, alpha = 1)

ax = plt.gca()
ax.axes.xaxis.set_ticks([])
ax.axes.yaxis.set_ticks([])

plt.savefig('distributions' + '_' + str(sample) + '.svg')


# # Example of median dynamics

# In[11]:


data_set = 'mNeon'

group = list(data_summary['sample'][data_summary['POI'] == data_set])

fig, ax = plt.subplots()
fig.set_size_inches(6.29/3, 1.75, forward=True)

# colormap
N = len(induction_levels)
cmap = plt.get_cmap('Blues', N)

# Normalizer
norm = mpl.colors.Normalize(vmin=np.min(data_summary['induction_levels']),                  vmax=np.max(data_summary['induction_levels']))

# creating ScalarMappable
# creating ScalarMappable
sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
sm.set_array([])

for sample in group:
    colorVal = sm.to_rgba(data_summary['induction_levels'][data_summary['sample'] == sample].values[0])    

    x = POI_median[sample]['time_norm']
        
    y = POI_median[sample]['IF']

    plt.plot(x, y,                '-o', MarkerSize = 3, color = colorVal, markerfacecolor='k')  

    plt.plot(x, y,                 '--', color = 'k', LineWidth = 0.2)

    plt.xlabel('Time (h)')
    plt.ylabel('Internal fluorescence (RPU)')
    plt.xlim(-2, 10)  
    plt.ylim(-0.001, 0.03)  
        
plt.savefig('IF_dynamics' + '_' + data_set + '.svg')


# # Example of secretion steady state data

# In[12]:


data_set = 'mNeon'

group = list(data_summary['sample'][data_summary['POI'] == data_set])

fig, ax = plt.subplots()
fig.set_size_inches(6.29/3, 1.75, forward=True)

# colormap
N = len(induction_levels)
cmap = plt.get_cmap('Blues', N)

# Normalizer
norm = mpl.colors.Normalize(vmin=np.min(data_summary['induction_levels']),                  vmax=np.max(data_summary['induction_levels']))

# creating ScalarMappable
# creating ScalarMappable
sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
sm.set_array([])

x_all = []
y_all = []

for sample in group:
    colorVal = sm.to_rgba(data_summary['induction_levels'][data_summary['sample'] == sample].values[0])    

    x = data_summary['induction_levels'][data_summary['sample'] == sample].values[0]
    
    x_all += [x]

    y = data_summary['sec'][data_summary['sample'] == sample].values[0]
    
    y_all += [y]

    plt.plot(x, y,                'o', MarkerSize = 5, markeredgecolor = 'k', color = colorVal)
    
model = np.polyfit(x_all, y_all, 2) 
predict = np.poly1d(model)
x_lin_reg = np.linspace(np.min(x_all), np.max(x_all))
y_lin_reg = predict(x_lin_reg)

colorVal = sm.to_rgba(0.5)    

plt.plot(x_lin_reg, y_lin_reg,         '--', color = colorVal, linewidth = 2, alpha = 0.5)

plt.xlabel('Norm. induction levels')
plt.ylabel('Norm. secretion levels at 24h')
plt.xlim(-0.05, 1)
plt.ylim(-0.5, 15)  
    
plt.savefig('SL' + '_' + data_set + '.svg')


# In[13]:


# Just a colorbar

fig, axs = plt.subplots(2, 1, figsize=(10, 2))
fig.subplots_adjust(hspace = .25, wspace=.25)
    
# colormap
N = len(induction_levels)
cmap = plt.get_cmap('Blues', N)

# Normalizer
norm = mpl.colors.Normalize(vmin=np.min(data_summary['induction_levels']),                  vmax=np.max(data_summary['induction_levels']))

# creating ScalarMappable
sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
sm.set_array([])

fig.colorbar(sm, label= "Norm. induction level", ax=axs[:], orientation= "horizontal", shrink=1)  
fig.suptitle(str(data_summary['POI'][group[0]]), fontsize=25)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




