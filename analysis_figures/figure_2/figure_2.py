#!/usr/bin/env python
# coding: utf-8

# ## Import modules and select data

# In[1]:


# Python modules
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.ticker import MaxNLocator
from matplotlib.offsetbox import AnchoredText
from matplotlib.ticker import FormatStrFormatter
from joypy import joyplot
import sys

# Main path
path = 'C:\\Users\\lifeware\\Desktop\\20221104_paper'

# In-house modules
sys.path.append(path + '\\code')

import getting_processed_data
import Parsing


# In[2]:


# IMPORT and PARSE data
# Write the experiment date in the order that you want them

mNeon = [20220412]

XylC = [20211130]

Xyl2 = [20211103]

hPON1 = [20220215]

scFv = [20210817, 20211115]

amy = [20210603, 20210830]

experiment_dates = mNeon + XylC + Xyl2 + hPON1 + scFv + amy

# Write the experiment (secretion measurement) date in the order that you want them (same as above)

mNeon = [20220414]

XylC = [20211202]

Xyl2 = [20211105]

hPON1 = [20220217]

scFv = [20210819, 20211117]

amy = [20210605, 20210831]

sec_experiment_dates = mNeon + XylC + Xyl2 + hPON1 + scFv + amy

# Secretion events threshold, below that, the secretion levels would be NaN
events_th = 10

# Order the data sets as you like
data_sets = ['mNeon', 'XylC', 'Xyl2', 'hPON1', 'scFv', 'amy']   

# if want to discard some sample, make a list of tupples for each (data set, replica, reactor), or enter ('None')

outliers = [('None')]

# Set an approximate time length of the experiment (the LED is on at time 0):
start = -3
end = 26


# In[3]:


# Here I get the data sets, tell if you use sensor strain
LED, cells_raw, cells_corrected, cells, POI, POI_median, metadata, sensor, sensor_median =         getting_processed_data.get_processed_data(experiment_dates, path, outliers)


# In[4]:


# Here I get the data sets only for secretion
sec_raw, sec = Parsing.sec_parse(experiment_dates, sec_experiment_dates, metadata, POI_median, path, events_th)

POI_median, POI_sec, induction_levels = Parsing.derived_data(sensor_median, POI_median, sec, metadata, end, path)


# In[5]:


# I am going to make a summary data frame for simplfy the data management

data_summary = metadata.copy()
        
# Here I compute steady state values of x[variable] between t_start and t_end
x = POI_median
variable = 'IF'
t_start = 20
t_end = 25
IF_ss = Parsing.steady_states(x, variable, t_start, t_end, metadata)

x = POI_median
variable = 'UPR'
UPR_ss = Parsing.steady_states(x, variable, t_start, t_end, metadata)

data_summary['induction_levels'] = list(induction_levels.values())
data_summary['IF_ss'] = IF_ss.copy()
    
POI_sec['sec'] = POI_sec['sec_raw'].copy()  

# and then use the proteomics data to scale it based on the experiment with same duty cycle

proteomics = pd.read_csv (path + '\\data\\proteomics_metric.csv')

for data_set in data_sets:
    
    sample = data_summary[data_summary['POI'] == data_set].sort_values(['DC']).iloc[-1]['sample']
    
    if np.isnan(POI_sec['sec'][POI_sec['sample'] == sample].values[0]) == True:
        
        sample = data_summary[data_summary['POI'] == data_set].sort_values(['DC']).iloc[-2]['sample']
    
    OM_factor = 10**proteomics['OM'][proteomics['POI'] == data_set].values[0]/POI_sec['sec'][POI_sec['sample'] == sample].values[0]   
    
    for sample in data_summary[data_summary['POI'] == data_set]['sample']:
        
        POI_sec['sec'].loc[POI_sec['sample'] == sample] = POI_sec['sec'].loc[POI_sec['sample'] == sample] * OM_factor
    
data_summary['sec'] = POI_sec['sec'].copy()
data_summary['UPR_ss'] = UPR_ss.copy()

# Also add induction levels to the POI_sec data frame
POI_sec['induction_levels'] = list(induction_levels.values())


# In[6]:


# Here I substract the fluorescence of secretion at induction 0, I count it as a blank 
    
group_0 = data_summary['sample'][data_summary['DC'] == 0]

for sample in group_0:

    blank = data_summary['sec'][data_summary['sample'] == sample].values[0]
    
    if np.isnan(blank) == False:
    
        replica = data_summary['replica'][data_summary['sample'] == sample].values[0]

        group = data_summary['sample'][data_summary['POI'] == data_summary['POI'][data_summary['sample'] == sample].values[0]]

        for sample_of_replica in group:

            if data_summary['replica'][data_summary['sample'] == sample_of_replica].values[0] == replica:

                data_summary['sec'].loc[data_summary['sample'] == sample_of_replica] =                 data_summary['sec'][data_summary['sample'] == sample_of_replica].copy() - blank

                POI_sec['sec'].loc[POI_sec['sample'] == sample_of_replica] =                 data_summary['sec'][data_summary['sample'] == sample_of_replica].copy()


# In[7]:


params = {'legend.fontsize': 8,
          'legend.loc' :'lower right',
          'legend.markerscale'   : 1,
          'axes.labelsize' : 8,
          'xtick.labelsize' : 8,
          'ytick.labelsize' : 8,
          'axes.titlesize' : 8,

          'font.family':  'Calibri',
          'font.style':   'normal',
          'font.variant': 'normal',
          'font.weight':  100,
          'font.stretch': 'normal',
          'font.size':    8.0,
          }

plt.rcParams.update(params)

features_plot = {}
features_plot['columns'] = [0, 1, 2]
features_plot['x_variable'] = ['time_norm', 'time_norm', 'induction_levels']
features_plot['y_variable'] = ['IF', 'UPR', 'sec']
features_plot['x_label'] = ['Time (h)', 'Time (h)', 'Norm. induction levels', 'Norm. induction levels']
features_plot['y_label'] = ['iPOI (RPU)', 'UPR (RPU)', 'Norm. sec. lev. $_{24h}$']
features_plot['x_lim'] = [[-3, 15], [-3, 15], [-0.05, 1.05], [-0.05, 1.05]]
features_plot['y_lim'] = [[0, 0.03], [0, 0.03], [0, 0.03], [0, 0.06], [0, 0.25], [0, 0.25]]
features_plot['y_lim_ss_POI'] = [[0, 0.04], [0, 0.04], [0, 0.02], [0, 0.06], [0, 0.06], [0, 0.06]]
features_plot['y_lim_ss_UPR'] = [[0, 0.04], [0, 0.08], [0, 0.2], [0, 0.2], [0, 0.4], [0, 0.6]]
features_plot['y_lim_sec'] = [[0, 20], [0, 20], [0, 4], [0, 2], [0, 2], [0, 2]]
features_plot['decimals'] = ['%.2f', '%.1f', '%.0f']

fig, axs = plt.subplots(len(data_sets), 4, figsize=(20, 5))
fig.subplots_adjust(hspace = 0.3, wspace=0.45)
fig.figsize = (6*5, 2*5)
fig.set_size_inches(8*0.85, 8, forward=True)

count_group = 0

for data_set in data_sets:
    group = list(data_summary['sample'][data_summary['POI'] == data_set])
    
    count_sample = 0
    
    x_all = []

    y_all = []
    
    for sample in group:
                        
        # colormap
        N = len(induction_levels)
        cmap = plt.get_cmap('Blues', N)

        # Normalizer
        norm = mpl.colors.Normalize(vmin=np.min(data_summary['induction_levels']),                          vmax=np.max(data_summary['induction_levels']))

        # creating ScalarMappable
        sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
        sm.set_array([])

        colorVal = sm.to_rgba(data_summary['induction_levels'][data_summary['sample'] == sample].values[0])

        for column in features_plot['columns']:
            
            ax = axs[count_group, column]

            if column == 2:

                list_of_x = data_summary[features_plot['x_variable'][column]][data_summary['POI'] == data_set] 
                list_of_y = data_summary[features_plot['y_variable'][column]][data_summary['POI'] == data_set]

                x = list(list_of_x)
                y = []

                for i in list_of_y:
                    y.append(i)

                ax.plot(x[count_sample], y[count_sample],                        'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = colorVal)
                ax.set_xlim(features_plot['x_lim'][column])
                ax.set_ylim(features_plot['y_lim_sec'][count_group][0], features_plot['y_lim_sec'][count_group][1])
                ax.set_yticks(np.arange(features_plot['y_lim_sec'][count_group][0], features_plot['y_lim_sec'][count_group][1],                                         features_plot['y_lim_sec'][count_group][1])/2)   
                ax.yaxis.set_major_locator(MaxNLocator(2)) 
                
                x_all = x
                y_all = y

            else:
                x = POI_median[sample][features_plot['x_variable'][column]]
                y = POI_median[sample][features_plot['y_variable'][column]]

                ax.plot(x, y,                        '-o', MarkerSize = 3, color = colorVal, markerfacecolor='k', label = data_set)
                ax.plot(x ,y,                        '--', color = 'k', LineWidth = 0.5, rasterized=True)
                
                ax.set_xlim(features_plot['x_lim'][column])

            if column == 1:
                ax.set_ylim(0, 0.6)
                ax.set_yticks(np.arange(0, 0.61, 0.6/2))    
                ax.yaxis.set_major_locator(MaxNLocator(2)) 

            if column == 0:
                ax.set_yticks(np.arange(features_plot['y_lim'][count_group][0], features_plot['y_lim'][count_group][1] * 1.1, features_plot['y_lim'][count_group][1]/2))

            if data_set != data_sets[-1]:
                ax.tick_params(
                axis='x',          # changes apply to the x-axis
                which='both',      # both major and minor ticks are affected
                bottom=True,      # ticks along the bottom edge are off
                top=False,         # ticks along the top edge are off
                labelbottom=False) # labels along the bottom edge are off 
                
            else:
                ax.set_xlabel(features_plot['x_label'][column])                

            ax.yaxis.set_major_formatter(FormatStrFormatter(features_plot['decimals'][column]))
            ax.set_ylabel(features_plot['y_label'][column])  

        count_sample = count_sample + 1
        
        if data_set == 'scFv':
            degree = 4
            
        else:
            degree = 2
        
        model = np.polyfit(x_all, y_all, degree) 
        predict = np.poly1d(model)
        x_lin_reg = np.linspace(0, np.max(x))
        y_lin_reg = predict(x_lin_reg)

        colorVal = sm.to_rgba(0.5)    
        
        ax.plot(x_lin_reg, y_lin_reg,                 '--', color = colorVal, linewidth = 1, alpha = 0.1)
        
    ax = axs[count_group, 3]

    if data_set == data_sets[0]:

        label_IF = 'iPOI'
        label_UPR = 'UPR'

    else:

        label_IF = None
        label_UPR = None

    y = data_summary['IF_ss'][data_summary['POI'] == data_set]

    x = data_summary['induction_levels'][data_summary['POI'] == data_set]

    ax.plot(x, y,                    'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = 'palegreen', label = label_IF)
    
    model = np.polyfit(x, y, 2) 
    predict = np.poly1d(model)
    x_lin_reg = np.linspace(0, np.max(x))
    y_lin_reg = predict(x_lin_reg)
        
    ax.plot(x_lin_reg, y_lin_reg,                 '--', color = 'g', linewidth = 1, alpha = 1)
    
    ax.set_ylabel('iPOI $_{24h}$ (RPU)')
        
    ax.set_xlim(features_plot['x_lim'][column])  
    
    ax2=ax.twinx()

    y = data_summary['UPR_ss'][data_summary['POI'] == data_set]

    x = data_summary['induction_levels'][data_summary['POI'] == data_set]

    ax2.plot(x, y,             'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = 'salmon', label = label_UPR)
    
    # Here I remove one outlier of UPR from mNeon, only to make the regression
    if data_set == 'mNeon':
        x = x.drop(1)
        y = y.drop(1)
    
    model = np.polyfit(x, y, 2) 
    predict = np.poly1d(model)
    x_lin_reg = np.linspace(0, np.max(x))
    y_lin_reg = predict(x_lin_reg)
            
    ax2.plot(x_lin_reg, y_lin_reg,                 '--', color = 'salmon', linewidth = 1, alpha = 1)
    

    ax2.set_ylabel('UPR $_{24h}$ (RPU)')

    if data_set != data_sets[-1]:
            ax.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=True,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off 

    else:
        ax.set_xlabel(features_plot['x_label'][column])   
        
    ax.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    ax.set_ylim(features_plot['y_lim_ss_POI'][count_group][0], features_plot['y_lim_ss_POI'][count_group][1])
    ax.set_yticks(np.arange(features_plot['y_lim_ss_POI'][count_group][0], features_plot['y_lim_ss_POI'][count_group][1],                             features_plot['y_lim_ss_POI'][count_group][1])/2)   
    ax.yaxis.set_major_locator(MaxNLocator(2)) 
        
    ax2.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    ax2.set_ylim(features_plot['y_lim_ss_UPR'][count_group][0], features_plot['y_lim_ss_UPR'][count_group][1])
    ax2.set_yticks(np.arange(features_plot['y_lim_ss_UPR'][count_group][0], features_plot['y_lim_ss_UPR'][count_group][1],                             features_plot['y_lim_ss_UPR'][count_group][1])/2)   
    ax2.yaxis.set_major_locator(MaxNLocator(2)) 
            
    count_group = count_group + 1
    
plt.savefig('data_summary.svg')


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




