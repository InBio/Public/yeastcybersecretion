#!/usr/bin/env python
# coding: utf-8

# ## Import modules and select data

# In[1]:


# Python modules
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.ticker import MaxNLocator
from matplotlib.offsetbox import AnchoredText
from matplotlib.ticker import FormatStrFormatter
from joypy import joyplot
from scipy.signal import savgol_filter
from scipy.optimize import curve_fit
import sys

# Main path
path = 'C:\\Users\\lifeware\\Desktop\\paper'

# In-house modules
sys.path.append(path + '\\code')

import getting_processed_data
import Parsing


# In[2]:


# IMPORT and PARSE data
# Write the experiment date in the order that you want them

mNeon = [20220412]

XylC = [20211130]

Xyl2 = [20211103]

hPON1 = [20220215]

scFv = [20210817, 20211115]

amy = [20210603, 20210830]

scFv_N = [20220608]

Xyl2_TM = [20220629]

experiment_dates = mNeon + XylC + Xyl2 + hPON1 + scFv + amy + scFv_N + Xyl2_TM

# Write the experiment (secretion measurement) date in the order that you want them (same as above)

mNeon = [20220414]

XylC = [20211202]

Xyl2 = [20211105]

hPON1 = [20220217]

scFv = [20210819, 20211117]

amy = [20210605, 20210831]

scFv_N = [20220610]

Xyl2_TM = [20220701]

sec_experiment_dates = mNeon + XylC + Xyl2 + hPON1 + scFv + amy + scFv_N + Xyl2_TM

# Secretion events threshold, below that, the secretion levels would be NaN
events_th = 10

# Order the data sets as you like
data_sets_charact = ['mNeon', 'XylC', 'Xyl2', 'hPON1', 'scFv', 'amy']   

data_sets = ['mNeon', 'XylC', 'Xyl2', 'hPON1', 'scFv', 'amy', 'scFv_N', 'Xyl2_TM']   

# if want to discard some sample, make a list of tupples for each (data set, replica, reactor), or enter ('None')

outliers = [('None')]

# Set an approximate time length of the experiment (the LED is on at time 0):
start = -3
end = 26


# In[3]:


# Here I get the data sets, tell if you use sensor strain
LED, cells_raw, cells_corrected, cells, POI, POI_median, metadata, sensor, sensor_median =         getting_processed_data.get_processed_data(experiment_dates, path, outliers)


# In[4]:


# I have to make this because no secretion data for Xyl2_TM in reactors 5 and 6

sec_metadata = metadata.copy()
sec_metadata = sec_metadata.drop(sec_metadata[(sec_metadata['POI'] == 'Xyl2_TM') & (sec_metadata['reactor'] == 5)].index)
sec_metadata = sec_metadata.drop(sec_metadata[(sec_metadata['POI'] == 'Xyl2_TM') & (sec_metadata['reactor'] == 6)].index)

sec_metadata


# In[5]:


# Here I get the data sets only for secretion
sec_raw, sec = Parsing.sec_parse(experiment_dates, sec_experiment_dates, sec_metadata, POI_median, path, events_th)

# I put to 0 those samples that I couldnt take
for sample in metadata['sample']:
        
    if (sample == metadata['sample'][(metadata['POI'] == 'Xyl2_TM') & (metadata['reactor'] == 5)].values[0]) or     (sample == metadata['sample'][(metadata['POI'] == 'Xyl2_TM') & (metadata['reactor'] == 6)].values[0]) or     (sample in list(metadata['sample'][(metadata['POI'] == 'scFv_HAC1')]))or     (sample in list(metadata['sample'][(metadata['POI'] == 'mNeon_HAC1')])):
        
        sec_raw[sample] = np.nan
        sec[sample] = np.nan
        
    else:
        
        pass
    
POI_median, POI_sec, induction_levels = Parsing.derived_data(sensor_median, POI_median, sec, metadata, end, path)


# In[6]:


# I am going to make a summary data frame for simplfy the data management

data_summary = metadata.copy()
        
# Here I compute steady state values of x[variable] between t_start and t_end
x = POI_median
variable = 'IF'
t_start = 20
t_end = 25
IF_ss = Parsing.steady_states(x, variable, t_start, t_end, metadata)

x = POI_median
variable = 'UPR'
UPR_ss = Parsing.steady_states(x, variable, t_start, t_end, metadata)

data_summary['induction_levels'] = list(induction_levels.values())
data_summary['IF_ss'] = IF_ss.copy()
data_summary['UPR_ss'] = UPR_ss.copy()

POI_sec['sec'] = POI_sec['sec_raw'].copy()  

# and then use the proteomics data to scale it based on the experiment with same duty cycle

proteomics = pd.read_csv (path + '\\data\\proteomics_metric.csv')

data_sets_special = ['scFv_N', 'Xyl2_TM']

reference_data_set = ['scFv', 'Xyl2']

for data_set in data_sets:
    
    if data_set in data_sets_special:
        
        reference = reference_data_set[data_sets_special.index(data_set)]

    else:
        
        reference = data_set
    
    sample = data_summary[data_summary['POI'] == reference].sort_values(['DC']).iloc[-1]['sample']
    
    if np.isnan(POI_sec['sec'][POI_sec['sample'] == sample].values[0]) == True:
        
        sample = data_summary[data_summary['POI'] == data_set].sort_values(['DC']).iloc[-2]['sample']
    
    # here is sec_raw because I use the Xyl2 to normalize
    OM_factor = 10**proteomics['OM'][proteomics['POI'] == reference].values[0]/POI_sec['sec_raw'][POI_sec['sample'] == sample].values[0]   
    
    for sample in data_summary[data_summary['POI'] == data_set]['sample']:
        
        POI_sec['sec'].loc[POI_sec['sample'] == sample] = POI_sec['sec'].loc[POI_sec['sample'] == sample] * OM_factor
    
data_summary['sec'] = POI_sec['sec'].copy()

# Also add induction levels to the POI_sec data frame
POI_sec['induction_levels'] = list(induction_levels.values())


# In[7]:


params = {'legend.fontsize': 8,
          'legend.loc' :'lower right',
          'legend.markerscale'   : 1,
          'axes.labelsize' : 8,
          'xtick.labelsize' : 8,
          'ytick.labelsize' : 8,
          'axes.titlesize' : 8,

          'font.family':  'Calibri',
          'font.style':   'normal',
          'font.variant': 'normal',
          'font.weight':  100,
          'font.stretch': 'normal',
          'font.size':    8.0,
          }

plt.rcParams.update(params)


# In[8]:


# colormap
N = len(induction_levels)
cmap = plt.get_cmap('Blues', N)

# Normalizer
norm = mpl.colors.Normalize(vmin=np.min(data_summary['induction_levels']),                  vmax=np.max(data_summary['induction_levels']))

# creating ScalarMappable
sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
sm.set_array([])


# In[9]:


# Fit Gaussians to infer the accumulators threshold

fig, axs = plt.subplots(16, len(data_sets), figsize=(25, 50))
fig.subplots_adjust(hspace = .5, wspace=0.1)

threshold_acc_IF = {}

count_group = 0

# Let's create a function to model and create data
def func(x, a, x0, sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))

for data_set in data_sets_charact:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels'])
    group = group_data_sorted['sample']
        
    count_sample = 0
    
    for sample in group:
        
        ax = axs[count_sample, count_group]
        
        # colormap
        colorVal = sm.to_rgba(0.5)
        
        x = POI[sample]['IF'][(POI[sample]['time_norm'] >= 20) & (POI[sample]['time_norm'] <= 25)]
        
        counts, bins, bars = ax.hist(x, bins = 100, color = colorVal)        
        
        # Executing curve_fit on noisy data
        try:
            popt, pcov = curve_fit(func, bins[1:], counts, maxfev=500000)
            
        except RuntimeError:
            print("Error - curve_fit failed")
            
        std_threshold = 3
            
        ym = func(bins[1:], popt[0], popt[1], popt[2])
        
        ax.plot(bins[1:], ym, c='r', label='Best fit')
        
        ax.plot([popt[1] + std_threshold*abs(popt[2]), popt[1] + std_threshold*abs(popt[2])], [0, np.max(counts)], '--', color = 'r')
                
        ax.set_title(str(data_summary['POI'][data_summary['sample'] == sample].values[0]) + ', ind = '                                 + str(round(induction_levels[sample], 2)))
        
        threshold_acc_IF[sample] = popt[1] + std_threshold*abs(popt[2])

        count_sample = count_sample + 1
        
    count_group = count_group + 1


# In[10]:


# Getting fraction of accululators

for data_set in data_sets_charact:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    count_sample = 0
    
    for sample in group:
        
        POI[sample]['n_cell'] = np.linspace(0, 1, len(POI[sample]))
        t_points = POI[sample].groupby(['time_norm'])
        
        lst = []
        
        for point in list(range(len(t_points))):

            t_point = t_points.get_group(list(t_points.groups)[point])
            
            lst.append(len(t_point['IF'][t_point['IF'] > threshold_acc_IF[sample]])/len(t_point['IF']))
                         
        POI_median[sample]['fraction_accumulators_IF'] = lst


# In[11]:


# Fit Gaussians to infer the accumulators threshold

fig, axs = plt.subplots(16, len(data_sets), figsize=(25, 50))
fig.subplots_adjust(hspace = .5, wspace=0.1)

threshold_acc_UPR = {}

count_group = 0

# Let's create a function to model and create data
def func(x, a, x0, sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))

for data_set in data_sets_charact:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels'])
    group = group_data_sorted['sample']
        
    count_sample = 0
    
    for sample in group:
        
        ax = axs[count_sample, count_group]
        
        # colormap
        colorVal = sm.to_rgba(0.5)
        
        x = POI[sample]['UPR'][(POI[sample]['time_norm'] >= 20) & (POI[sample]['time_norm'] <= 25)]
        
        counts, bins, bars = ax.hist(x, bins = 100, color = colorVal)        
        
        # Executing curve_fit on noisy data
        try:
            popt, pcov = curve_fit(func, bins[1:], counts, maxfev=500000)
            
        except RuntimeError:
            print("Error - curve_fit failed")
            
        std_threshold = 3
            
        ym = func(bins[1:], popt[0], popt[1], popt[2])
        
        ax.plot(bins[1:], ym, c='r', label='Best fit')
        
        ax.plot([popt[1] + std_threshold*abs(popt[2]), popt[1] + std_threshold*abs(popt[2])], [0, np.max(counts)], '--', color = 'r')
                
        ax.set_title(str(data_summary['POI'][data_summary['sample'] == sample].values[0]) + ', ind = '                                 + str(round(induction_levels[sample], 2)))
        
        threshold_acc_UPR[sample] = popt[1] + std_threshold*abs(popt[2])

        count_sample = count_sample + 1
        
    count_group = count_group + 1


# In[12]:


# Getting fraction of accululators

for data_set in data_sets_charact:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    count_sample = 0
    
    for sample in group:
        
        POI[sample]['n_cell'] = np.linspace(0, 1, len(POI[sample]))
        t_points = POI[sample].groupby(['time_norm'])
        
        lst = []
        
        for point in list(range(len(t_points))):

            t_point = t_points.get_group(list(t_points.groups)[point])
            
            lst.append(len(t_point['UPR'][t_point['UPR'] > threshold_acc_UPR[sample]])/len(t_point['UPR']))
                         
        POI_median[sample]['fraction_accumulators_UPR'] = lst


# In[13]:


# Getting fraction of accululators with potential to recover

for data_set in data_sets_charact:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    count_sample = 0
    
    for sample in group:
        
        POI[sample]['n_cell'] = np.linspace(0, 1, len(POI[sample]))
        t_points = POI[sample].groupby(['time_norm'])
        
        lst = []
        
        for point in list(range(len(t_points))):

            t_point = t_points.get_group(list(t_points.groups)[point])
            
            lst.append(len(t_point['IF'][(t_point['IF'] > threshold_acc_IF[sample]) & (t_point['UPR'] > threshold_acc_UPR[sample])]) / len(t_point['UPR']))
                         
        POI_median[sample]['fraction_accumulators'] = lst


# In[14]:


# Getting fraction of accululators

data_sets_selected = ['scFv_N', 'Xyl2_TM']

reference_data_set = ['scFv', 'Xyl2']

for data_set in data_sets_selected:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    count_sample = 0
    
    for sample in group:
        
        IL = data_summary['induction_levels'][data_summary['sample'] == sample].values[0]
        
        if np.isnan(IL) == False:
        
            IL_reference = list(data_summary['induction_levels'][data_summary['POI'] == reference_data_set[data_sets_selected.index(data_set)]])

            abs_values = list(abs(np.array(IL_reference)-IL))

            idx = abs_values.index(np.nanmin(abs_values))

            sample_ref = data_summary['sample'][data_summary['POI'] == reference_data_set[data_sets_selected.index(data_set)]].iloc[idx]

            POI[sample]['n_cell'] = np.linspace(0, 1, len(POI[sample]))
            t_points = POI[sample].groupby(['time_norm'])

            lst = []

            for point in list(range(len(t_points))):

                t_point = t_points.get_group(list(t_points.groups)[point])

                lst.append(len(t_point['IF'][t_point['IF'] > threshold_acc_IF[sample_ref]])/len(t_point['IF']))


            POI_median[sample]['fraction_accumulators_IF'] = lst      
            
        else:
            
            POI_median[sample]['fraction_accumulators_IF'] = [np.nan] * POI_median[sample]['IF']


# In[15]:


#plot distributions
data_sets_selected = ['scFv', 'amy']

target = 1

fig, axs = plt.subplots(1, len(data_sets_selected))
fig.set_size_inches(8*0.9, 1.2, forward=True)
fig.subplots_adjust(hspace = 0, wspace=0)

variable = 'IF'

if variable == 'IF':
    color = 'limegreen'
    yaxis = 'iPOI'
if variable == 'UPR':
    color = 'salmon'
    yaxis = 'UPR'

count_group = 0

induction_selected = {}

for data_set in data_sets_selected:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels']) 
    
    closest_list = list(abs(group_data_sorted['induction_levels'].values - target))

    idx = closest_list.index(np.min(closest_list))

    group = [group_data_sorted['sample'].iloc[idx]]
    
    count_sample = 0
    
    induction_selected[data_set] = []
    
    for sample in group:
        
        ax = axs[count_group]
                
        induction_selected[data_set] += [data_summary['induction_levels'][data_summary['sample'] == sample].values[0]]
        
        colorVal = sm.to_rgba(data_summary['induction_levels'][data_summary['sample'] == sample].values[0])
        
        fraction_max = np.max(POI_median[sample]['fraction_accumulators_IF'][(POI_median[sample]['time_norm'] > 0) & (POI_median[sample]['time_norm'] <= 25)])
        
        t_frac_max = POI_median[sample]['time_norm'][POI_median[sample]['fraction_accumulators_IF'] == fraction_max].values[0]
        
        if count_group != 0:    
            ax.tick_params(
            axis='y',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            left=False,      # ticks along the bottom edge are off
            right=False,         # ticks along the top edge are off
            labelleft=False) # labels along the bottom edge are off
        
        POI[sample]['n_cell'] = np.linspace(0,1,len(POI[sample]))
        t_points = POI[sample].groupby(['time_norm'])
                
        for point in list(range(len(t_points))):

            t_point = t_points.get_group(list(t_points.groups)[point])
            
            x = t_point['time_norm'] * np.ones(len(t_point[variable])) + np.random.normal(scale=0.1, size=len(t_point[variable]))

            y = t_point[variable]
            
            if t_point['time_norm'].iloc[0] == t_frac_max:
                
                color_max = colorVal
            
            else:
                
                color_max = color
                                
            ax.scatter(x, y,                         s=1, facecolors='none', color = color_max, alpha=0.05, rasterized=True)
        
        axs[0].set_ylabel(yaxis + ' (RPU)')
        ax.set_yscale('log')
#         ax.set_xlim(-3, 24)
        ax.set_ylim(10**-4, 10**1)
        ax.set_xlim(-3, 24)
        
        count_sample = count_sample + 1
        
    count_group = count_group + 1
    
plt.savefig('pop_iPOI.svg')


# In[16]:


#plot growth rate

target = 1

fig, axs = plt.subplots(1, len(data_sets_selected))
fig.set_size_inches(2.5, 0.3, forward=True)
fig.subplots_adjust(hspace = 0, wspace=0)

count_group = 0

for data_set in data_sets_selected:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels']) 
    
    closest_list = list(abs(group_data_sorted['induction_levels'].values - target))

    idx = closest_list.index(np.min(closest_list))

    group = [group_data_sorted['sample'].iloc[idx]]
    
    count_sample = 0
    
    for sample in group:
        
        ax = axs[count_group]
            
        x = POI_median[sample]['time_norm']
        
        y = POI_median[sample]['gr']
        
        ax.plot(x, y,                 '-', color = 'k')
        
        ax.plot([-5, 24], [0.4 ,0.4],                 '--', color = 'k', LineWidth = 1)
        
        ax.plot([0, 0], [0 ,0.5],                 '--', color = 'k', LineWidth = 1)
        
        ax.set_xlim(-3, 15)
        ax.set_ylim(0.2, 0.5)
        
        count_sample = count_sample + 1
        
    count_group = count_group + 1

plt.savefig('gr.svg')


# In[17]:


#plot distributions
data_sets_selected = ['scFv', 'amy']

target = 1

fig, axs = plt.subplots(1, len(data_sets_selected))
fig.set_size_inches(8*0.9, 1.2, forward=True)
fig.subplots_adjust(hspace = 0, wspace=0)

variable = 'UPR'

if variable == 'IF':
    color = 'limegreen'
    yaxis = 'iPOI'
if variable == 'UPR':
    color = 'salmon'
    yaxis = 'UPR'

count_group = 0

induction_selected = {}

for data_set in data_sets_selected:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    if data_set == 'mNeon':
        group = group[group['replica'] == 1]
    
    group_data_sorted = group.sort_values(by=['induction_levels']) 
    
    closest_list = list(abs(group_data_sorted['induction_levels'].values - target))

    idx = closest_list.index(np.min(closest_list))

    group = [group_data_sorted['sample'].iloc[idx]]
    
    count_sample = 0
    
    induction_selected[data_set] = []
    
    for sample in group:
        
        ax = axs[count_group]
                
        induction_selected[data_set] += [data_summary['induction_levels'][data_summary['sample'] == sample].values[0]]
        
        if count_group != 0:    
            ax.tick_params(
            axis='y',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            left=False,      # ticks along the bottom edge are off
            right=False,         # ticks along the top edge are off
            labelleft=False) # labels along the bottom edge are off
        
        POI[sample]['n_cell'] = np.linspace(0,1,len(POI[sample]))
        t_points = POI[sample].groupby(['time_norm'])
                
        for point in list(range(len(t_points))):

            t_point = t_points.get_group(list(t_points.groups)[point])
            
            x = t_point['time_norm'] * np.ones(len(t_point[variable])) + np.random.normal(scale=0.1, size=len(t_point[variable]))

            y = t_point[variable]
                    
            ax.scatter(x, y,                         s=1, facecolors='none', color = color, alpha=0.05, rasterized=True)
        
        axs[0].set_ylabel(yaxis + ' (RPU)')
        ax.set_xlabel('Time (h)')
        ax.set_yscale('log')
#         ax.set_xlim(-3, 24)
        ax.set_ylim(10**-4, 10**1)
        ax.set_xlim(-3, 24)
        
        count_sample = count_sample + 1
        
    count_group = count_group + 1
    
plt.savefig('pop_UPR.svg')


# In[18]:


# Correlation overshooted population is maximal vs induction

data_sets_selected = data_sets_charact

fig, axs = plt.subplots(1, len(data_sets_selected))
fig.set_size_inches(8*0.9, 1.7, forward=True)
fig.subplots_adjust(hspace = 0, wspace=0)

count_group = 0

for data_set in data_sets_selected:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels'])
    group = group_data_sorted['sample']
    
    count_sample = 0
    
    ax = axs[count_group]

    for sample in group:
        
        # colormap
        N = len(induction_levels)
        cmap = plt.get_cmap('Blues', N)

        # Normalizer
        norm = mpl.colors.Normalize(vmin=np.min(data_summary['induction_levels']),                          vmax=np.max(data_summary['induction_levels']))

        # creating ScalarMappable
        sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
        sm.set_array([])

        colorVal = sm.to_rgba(data_summary['induction_levels'][data_summary['sample'] == sample].values[0])

        fraction_max = np.max(POI_median[sample]['fraction_accumulators_IF'][(POI_median[sample]['time_norm'] > 0) & (POI_median[sample]['time_norm'] <= 25)])
        
        x = data_summary['induction_levels'][data_summary['sample'] == sample].values[0]
        
        y = fraction_max

        axs[count_group].plot(x, y,                      'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = colorVal)
        
        ax.set_xlabel('Norm. induction levels')

        count_sample = count_sample + 1
        
    axs[0].set_ylabel('Maximal fraction accumulators')

    ax.set_xlim(-0.1, 1.1)
    ax.set_ylim(-0.1, 1.1)
    
    if count_group != 0:    
        ax.tick_params(
        axis='y',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        left=False,      # ticks along the bottom edge are off
        right=False,         # ticks along the top edge are off
        labelleft=False) # labels along the bottom edge are off
        
    ax.text(0, 1, data_set)
        
    count_group = count_group + 1
    
plt.savefig('acc_vs_ind.svg')


# In[19]:


# Prepare extra details for next plot

nutrients = {1: 5, 2: 50, 3: 5000, 4: 500, 5: 5, 6: 50, 7: 5000, 8: 500}

data_summary['nutrients'] = [5000] * len(data_summary)

for sample in data_summary['sample'][data_summary['POI'] == 'scFv_N']:
    
    data_summary['nutrients'][data_summary['sample'] == sample] = nutrients[data_summary['reactor'][data_summary['sample'] == sample].values[0]]
    
data_summary


# In[20]:


data_sets_selected = ['scFv', 'scFv_N']

fig, axs = plt.subplots(1, 1)
fig.set_size_inches(8*0.9/4, 1.7, forward=True)
fig.subplots_adjust(hspace = 0, wspace=0)

for data_set in data_sets_selected:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
        
    ax = plt
    
    if data_set == 'scFv':
        color = 'Greens'
        
    else:
        color = 'Oranges'
    
    # colormap
    N = len(list(set(list(data_summary['nutrients'].values))))
    cmap = plt.get_cmap(color, N)

    # Normalizer
    norm = mpl.colors.Normalize(vmin = np.log10(np.min(data_summary['nutrients'])),                          vmax = np.log10(np.max(data_summary['nutrients'])))

    # creating ScalarMappable
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])

    for sample in group:
        
        colorVal = sm.to_rgba(np.log10(data_summary['nutrients'][data_summary['sample'] == sample].values[0]))

        fraction_max = np.max(POI_median[sample]['fraction_accumulators_IF'][(POI_median[sample]['time_norm'] > 0) & (POI_median[sample]['time_norm'] <= 25)])

        x = data_summary['DC'][data_summary['sample'] == sample].values[0]

        y = fraction_max

        ax.plot(x, y,                      'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = colorVal)
        
ax.xlabel('Norm. production demand')
ax.ylabel('Maximal fraction accumulators')
ax.xlim(-0.1, 1.1)
ax.ylim(-0.1, 1.1)

plt.savefig('acc_vs_DC_N.svg')        


# In[21]:


data_sets_selected = ['scFv', 'scFv_N']

fig, axs = plt.subplots(1, 1)
fig.set_size_inches(8*0.9/4, 1.7, forward=True)
fig.subplots_adjust(hspace = 0, wspace=0)

for data_set in data_sets_selected:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
        
    ax = plt
    
    if data_set == 'scFv':
        color = 'Greens'
        
    else:
        color = 'Oranges'
    
    # colormap
    N = len(list(set(list(data_summary['nutrients'].values))))
    cmap = plt.get_cmap(color, N)

    # Normalizer
    norm = mpl.colors.Normalize(vmin = np.log10(np.min(data_summary['nutrients'])),                          vmax = np.log10(np.max(data_summary['nutrients'])))

    # creating ScalarMappable
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])

    for sample in group:
        
        colorVal = sm.to_rgba(np.log10(data_summary['nutrients'][data_summary['sample'] == sample].values[0]))

        fraction_max = np.max(POI_median[sample]['fraction_accumulators_IF'][(POI_median[sample]['time_norm'] > 0) & (POI_median[sample]['time_norm'] <= 25)])

        x = data_summary['induction_levels'][data_summary['sample'] == sample].values[0]

        y = fraction_max

        ax.plot(x, y,                      'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = colorVal)
        
ax.xlabel('Norm. induction levels')
ax.ylabel('Maximal fraction accumulators')
ax.xlim(-0.1, 1.1)
ax.ylim(-0.1, 1.1)

plt.savefig('acc_vs_ind_N.svg')        


# In[22]:


data_sets_selected = ['Xyl2', 'Xyl2_TM']

fig, axs = plt.subplots(1, 1)
fig.set_size_inches(8*0.9/4, 1.7, forward=True)
fig.subplots_adjust(hspace = 0, wspace=0)

for data_set in data_sets_selected:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
        
    ax = plt
    
    if data_set == 'Xyl2':
        color = 'Greens'
        
    else:
        color = 'Purples'
    
    # colormap
    N = len(induction_levels)
    cmap = plt.get_cmap(color, N)

    # Normalizer
    norm = mpl.colors.Normalize(vmin=np.min(data_summary['induction_levels']),                      vmax=np.max(data_summary['induction_levels']))

    # creating ScalarMappable
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])

    for sample in group:
        
        if np.isnan(data_summary['induction_levels'][data_summary['sample'] == sample].values[0]) == False:
        
            colorVal = sm.to_rgba(0.75)

            fraction_max = np.nanmax(POI_median[sample]['fraction_accumulators_IF'][(POI_median[sample]['time_norm'] > 0) & (POI_median[sample]['time_norm'] <= 25)])

            x = data_summary['induction_levels'][data_summary['sample'] == sample].values[0]

            y = fraction_max

            ax.plot(x, y,                          'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = colorVal)
        
ax.xlabel('Norm. induction levels')
ax.ylabel('Maximal fraction accumulators')
ax.xlim(-0.1, 1.1)
ax.ylim(-0.1, 1.1)

plt.savefig('acc_vs_ind_TM.svg')        


# In[23]:


data_sets_selected = ['Xyl2', 'Xyl2_TM']

fig, axs = plt.subplots(1, 1)
fig.set_size_inches(8*0.9/4, 1.7, forward=True)
fig.subplots_adjust(hspace = 0, wspace=0)

for data_set in data_sets_selected:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
        
    ax = plt
    
    if data_set == 'Xyl2':
        color = 'Greens'
        
    else:
        color = 'Purples'
    
    # colormap
    N = len(induction_levels)
    cmap = plt.get_cmap(color, N)

    # Normalizer
    norm = mpl.colors.Normalize(vmin=np.min(data_summary['induction_levels']),                      vmax=np.max(data_summary['induction_levels']))

    # creating ScalarMappable
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])

    for sample in group:
    
        colorVal = sm.to_rgba(0.75)    

        x = data_summary['induction_levels'][data_summary['sample'] == sample].values[0]

        y = data_summary['sec'][data_summary['sample'] == sample].values[0]

        plt.plot(x, y,                    'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = colorVal)

plt.xlabel('Norm. induction levels')
plt.ylabel('Norm. sec. lev. $_{24h}$')
# plt.xlim(-0.05, 1)
# plt.ylim(-0.5, 5)  
    
plt.savefig('SL' + '_' + data_set + '.svg')


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




