#!/usr/bin/env python
# coding: utf-8

# ## modules and select data

# In[1]:


# Python modules
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.ticker import MaxNLocator
from matplotlib.offsetbox import AnchoredText
from matplotlib.ticker import FormatStrFormatter
from joypy import joyplot
from scipy.signal import savgol_filter
from scipy.optimize import curve_fit
from sklearn.mixture import GaussianMixture
import cma
import json
import sys

# Main path
path = 'C:\\Users\\lifeware\\Desktop\\paper'

# In-house modules
sys.path.append(path + '\\code')

import getting_processed_data
import Parsing
import simu


# In[2]:


# IMPORT and PARSE data
# Write the experiment date in the order that you want them

mNeon_non_sec = [20211027] 

mNeon = [20220412]

XylC = [20211130]

Xyl2 = [20211103]

hPON1 = [20220215]

scFv = [20210817, 20211115]

amy = [20210603, 20210830]

experiment_dates = mNeon_non_sec + mNeon + XylC + Xyl2 + hPON1 + scFv + amy

# Write the experiment (secretion measurement) date in the order that you want them (same as above)

mNeon_non_sec = [20211029] 

mNeon = [20220414]

XylC = [20211202]

Xyl2 = [20211105]

hPON1 = [20220217]

scFv = [20210819, 20211117]

amy = [20210605, 20210831]

sec_experiment_dates = mNeon_non_sec + mNeon + XylC + Xyl2 + hPON1 + scFv + amy

# Secretion events threshold, below that, the secretion levels would be NaN
events_th = 10

# Order the data sets as you like
data_sets_charact = ['mNeon-non-sec', 'mNeon', 'XylC', 'Xyl2', 'hPON1', 'scFv', 'amy']   

data_sets = data_sets_charact  

# if want to discard some sample, make a list of tupples for each (data set, replica, reactor), or enter ('None')

outliers = [('None')]

# Set an approximate time length of the experiment (the LED is on at time 0):
start = -3
end = 26


# In[3]:


# Here I get the data sets, tell if you use sensor strain
LED, cells_raw, cells_corrected, cells, POI, POI_median, metadata, sensor, sensor_median =         getting_processed_data.get_processed_data(experiment_dates, path, outliers)


# In[4]:


# Here I get the data sets only for secretion
sec_raw, sec = Parsing.sec_parse(experiment_dates, sec_experiment_dates, metadata, POI_median, path, events_th)

POI_median, POI_sec, induction_levels = Parsing.derived_data(sensor_median, POI_median, sec, metadata, end, path)


# In[5]:


# I am going to make a summary data frame for simplfy the data management

data_summary = metadata.copy()
        
# Here I compute steady state values of x[variable] between t_start and t_end
x = POI_median
variable = 'IF'
t_start = 20
t_end = 25
IF_ss = Parsing.steady_states(x, variable, t_start, t_end, metadata)

x = POI_median
variable = 'UPR'
UPR_ss = Parsing.steady_states(x, variable, t_start, t_end, metadata)

data_summary['induction_levels'] = list(induction_levels.values())
data_summary['IF_ss'] = IF_ss.copy()
    
POI_sec['sec'] = POI_sec['sec_raw'].copy()  

# and then use the proteomics data to scale it based on the experiment with same duty cycle

proteomics = pd.read_csv (path + '\\data\\proteomics_metric.csv')

for data_set in data_sets:
    
    sample = data_summary[data_summary['POI'] == data_set].sort_values(['DC']).iloc[-1]['sample']
    
    if np.isnan(POI_sec['sec'][POI_sec['sample'] == sample].values[0]) == True:
        
        sample = data_summary[data_summary['POI'] == data_set].sort_values(['DC']).iloc[-2]['sample']
    
    OM_factor = 10**proteomics['OM'][proteomics['POI'] == data_set].values[0]/POI_sec['sec'][POI_sec['sample'] == sample].values[0]   
    
    for sample in data_summary[data_summary['POI'] == data_set]['sample']:
        
        POI_sec['sec'].loc[POI_sec['sample'] == sample] = POI_sec['sec'].loc[POI_sec['sample'] == sample] * OM_factor
    
data_summary['sec'] = POI_sec['sec'].copy()
data_summary['UPR_ss'] = UPR_ss.copy()

# Also add induction levels to the POI_sec data frame
POI_sec['induction_levels'] = list(induction_levels.values())


# In[6]:


# Here I substract the fluorescence of secretion at induction 0, I count it as a blank 
    
group_0 = data_summary['sample'][data_summary['DC'] == 0]

for sample in group_0:

    blank = data_summary['sec'][data_summary['sample'] == sample].values[0]
    
    if np.isnan(blank) == False:
    
        replica = data_summary['replica'][data_summary['sample'] == sample].values[0]

        group = data_summary['sample'][data_summary['POI'] == data_summary['POI'][data_summary['sample'] == sample].values[0]]

        for sample_of_replica in group:

            if data_summary['replica'][data_summary['sample'] == sample_of_replica].values[0] == replica:

                data_summary['sec'].loc[data_summary['sample'] == sample_of_replica] =                 data_summary['sec'][data_summary['sample'] == sample_of_replica].copy() - blank

                POI_sec['sec'].loc[POI_sec['sample'] == sample_of_replica] =                 data_summary['sec'][data_summary['sample'] == sample_of_replica].copy()


# In[7]:


params = {'legend.fontsize': 8,
          'legend.loc' :'lower right',
          'legend.markerscale'   : 1,
          'axes.labelsize' : 8,
          'xtick.labelsize' : 8,
          'ytick.labelsize' : 8,
          'axes.titlesize' : 8,

          'font.family':  'Calibri',
          'font.style':   'normal',
          'font.variant': 'normal',
          'font.weight':  100,
          'font.stretch': 'normal',
          'font.size':    8.0,
          }

plt.rcParams.update(params)


# In[8]:


# colormap
N = len(induction_levels)
cmap = plt.get_cmap('Blues', N)

# Normalizer
norm = mpl.colors.Normalize(vmin=np.min(data_summary['induction_levels']),                  vmax=np.max(data_summary['induction_levels']))

# creating ScalarMappable
sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
sm.set_array([])


# In[9]:


# Fit Gaussians to infer the accumulators threshold

fig, axs = plt.subplots(16, len(data_sets), figsize=(25, 50))
fig.subplots_adjust(hspace = .5, wspace=0.1)

threshold_acc_IF = {}

count_group = 0

# Let's create a function to model and create data
def func(x, a, x0, sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))

for data_set in data_sets_charact:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels'])
    group = group_data_sorted['sample']
        
    count_sample = 0
    
    for sample in group:
        
        ax = axs[count_sample, count_group]
        
        # colormap
        colorVal = sm.to_rgba(0.5)
        
        x = POI[sample]['IF'][(POI[sample]['time_norm'] >= 20) & (POI[sample]['time_norm'] <= 25)]
        
        counts, bins, bars = ax.hist(x, bins = 100, color = colorVal)        
        
        # Executing curve_fit on noisy data
        try:
            popt, pcov = curve_fit(func, bins[1:], counts, maxfev=500000)
            
        except RuntimeError:
            print("Error - curve_fit failed")
            
        std_threshold = 3
            
        ym = func(bins[1:], popt[0], popt[1], popt[2])
        
        ax.plot(bins[1:], ym, c='r', label='Best fit')
        
        ax.plot([popt[1] + std_threshold*abs(popt[2]), popt[1] + std_threshold*abs(popt[2])], [0, np.max(counts)], '--', color = 'r')
                
        ax.set_title(str(data_summary['POI'][data_summary['sample'] == sample].values[0]) + ', ind = '                                 + str(round(induction_levels[sample], 2)))
        
        threshold_acc_IF[sample] = popt[1] + std_threshold*abs(popt[2])

        count_sample = count_sample + 1
        
    count_group = count_group + 1


# In[10]:


# Getting fraction of accululators

for data_set in data_sets_charact:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    count_sample = 0
    
    for sample in group:
        
        POI[sample]['n_cell'] = np.linspace(0, 1, len(POI[sample]))
        t_points = POI[sample].groupby(['time_norm'])
        
        lst = []
        
        for point in list(range(len(t_points))):

            t_point = t_points.get_group(list(t_points.groups)[point])
            
            lst.append(len(t_point['IF'][t_point['IF'] > threshold_acc_IF[sample]])/len(t_point['IF']))
                         
        POI_median[sample]['fraction_accumulators_IF'] = lst


# In[11]:


# Fit Gaussians to infer the accumulators threshold

fig, axs = plt.subplots(16, len(data_sets), figsize=(25, 50))
fig.subplots_adjust(hspace = .5, wspace=0.1)

threshold_acc_UPR = {}

count_group = 0

# Let's create a function to model and create data
def func(x, a, x0, sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))

for data_set in data_sets_charact:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels'])
    group = group_data_sorted['sample']
        
    count_sample = 0
    
    for sample in group:
        
        ax = axs[count_sample, count_group]
        
        # colormap
        colorVal = sm.to_rgba(0.5)
        
        x = POI[sample]['UPR'][(POI[sample]['time_norm'] >= 20) & (POI[sample]['time_norm'] <= 25)]
        
        counts, bins, bars = ax.hist(x, bins = 100, color = colorVal)        
        
        # Executing curve_fit on noisy data
        try:
            popt, pcov = curve_fit(func, bins[1:], counts, maxfev=500000)
            
        except RuntimeError:
            print("Error - curve_fit failed")
            
        std_threshold = 3
            
        ym = func(bins[1:], popt[0], popt[1], popt[2])
        
        ax.plot(bins[1:], ym, c='r', label='Best fit')
        
        ax.plot([popt[1] + std_threshold*abs(popt[2]), popt[1] + std_threshold*abs(popt[2])], [0, np.max(counts)], '--', color = 'r')
                
        ax.set_title(str(data_summary['POI'][data_summary['sample'] == sample].values[0]) + ', ind = '                                 + str(round(induction_levels[sample], 2)))
        
        threshold_acc_UPR[sample] = popt[1] + std_threshold*abs(popt[2])

        count_sample = count_sample + 1
        
    count_group = count_group + 1


# In[12]:


# Getting fraction of accululators

for data_set in data_sets_charact:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    count_sample = 0
    
    for sample in group:
        
        POI[sample]['n_cell'] = np.linspace(0, 1, len(POI[sample]))
        t_points = POI[sample].groupby(['time_norm'])
        
        lst = []
        
        for point in list(range(len(t_points))):

            t_point = t_points.get_group(list(t_points.groups)[point])
            
            lst.append(len(t_point['UPR'][t_point['UPR'] > threshold_acc_UPR[sample]])/len(t_point['UPR']))
                         
        POI_median[sample]['fraction_accumulators_UPR'] = lst


# In[13]:


# Getting fraction of accululators with potential to recover

for data_set in data_sets_charact:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    count_sample = 0
    
    for sample in group:
        
        POI[sample]['n_cell'] = np.linspace(0, 1, len(POI[sample]))
        t_points = POI[sample].groupby(['time_norm'])
        
        lst = []
        
        for point in list(range(len(t_points))):

            t_point = t_points.get_group(list(t_points.groups)[point])
            
            lst.append(len(t_point['IF'][(t_point['IF'] > threshold_acc_IF[sample]) & (t_point['UPR'] > threshold_acc_UPR[sample])]) / len(t_point['UPR']))
                         
        POI_median[sample]['fraction_accumulators'] = lst


# In[14]:


# Correlation overshooted population is maximal vs induction
data_sets_selected = ['scFv', 'amy']

fig, axs = plt.subplots(len(data_sets_selected), 1)
fig.set_size_inches(8*0.25, 1.7*2, forward=True)
fig.subplots_adjust(hspace = 0.1, wspace=0.2)

count_group = 0

for data_set in data_sets_selected:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels'])
    group = group_data_sorted['sample']
    
    count_sample = 0
    
    ax = axs[count_group]
    
    x_all = []
    y_all = []
    
    for sample in group:
        
        colorVal = sm.to_rgba(data_summary['induction_levels'][data_summary['sample'] == sample].values[0])

        fraction_max = np.max(POI_median[sample]['fraction_accumulators_UPR'][(POI_median[sample]['time_norm'] > 0) & (POI_median[sample]['time_norm'] < 24)])
        
        x = fraction_max
        
        y = data_summary['sec'][data_summary['sample'] == sample].values[0]
        
        if (data_summary['DC'][data_summary['sample'] == sample].values[0] != 0) &         (np.isnan(data_summary['sec'][data_summary['sample'] == sample].values[0]) == False) :
            x_all += [x]
            y_all += [y]

        axs[count_group].plot(x, y,                      'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = colorVal)

        count_sample = count_sample + 1
        
    if (data_set == 'scFv') or (data_set == 'amy'):
        
        if data_set == 'scFv':
            degree = 4
        if data_set == 'amy':
            degree = 2
        
    model = np.polyfit([0] + x_all, [0] + y_all, degree) 
    predict = np.poly1d(model)
    x_lin_reg = np.linspace(0, np.max(x_all))
    y_lin_reg = predict(x_lin_reg)

    ax.plot(x_lin_reg, y_lin_reg,                 '--', color = sm.to_rgba(0.5), linewidth = 1, alpha = 1)   
    
    if data_set == data_sets_selected[0]:
        ax.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=True,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off 

    ax.set_xlim(-0.01, 0.3)
    ax.set_ylim(-0.01, 2)
    
    ax.set_ylabel('Norm. sec. lev. $_{24h}$')
    
    ax.text(0, 1.75 ,data_set)
    
    count_group = count_group + 1
    
axs[1].set_xlabel('Maximal fraction of UPR-defined accumulators')
    
plt.savefig('acc_vs_sec.svg')


# In[15]:


def acc_func(UPR, th, slp, acc_basal):
        
    acc = []
        
    for x in UPR:
                
        if x <= th:
            
            y = acc_basal

        if x > th:
        
            y = acc_basal + slp * (x - th) 
            
        acc += [y]        
    
    return acc


# In[16]:


def acc_to_cap_func(acc, acc_basal):
    
    acc = np.array(acc)
        
    cap = acc - acc_basal
        
    return cap


# In[17]:



data_sets_selected = ['scFv', 'amy']

popt = {}

count_group = 0

for data_set in data_sets_selected:
    
    group = data_summary[(data_summary['POI'] == data_set) & (data_summary['induction_levels'] > 0)]
    group_data_sorted = group.sort_values(by=['induction_levels'])
    group = group_data_sorted['sample']
    
    x = []
    y = []
    
    for sample in group:
                
        x += [data_summary['induction_levels'][data_summary['sample'] == sample].values[0]]
                
        y += [np.max(POI_median[sample]['fraction_accumulators_UPR'][(POI_median[sample]['time_norm'] > 0) & (POI_median[sample]['time_norm'] < 24)])]      

    p = np.array([0.5, 1, 0.05])

    xdata = np.array(x)
    ydata = np.array(y)

    popt[count_group], pcov = curve_fit(acc_func, xdata, ydata, p)
    
    count_group = count_group + 1


# In[18]:


count_group = 0

acc = {}
cap = {}

for data_set in data_sets_selected:
    
    group = data_summary[(data_summary['POI'] == data_set) & (data_summary['induction_levels'] > 0)]
    group_data_sorted = group.sort_values(by=['induction_levels'])
    group = group_data_sorted['sample']
    
    x = []
    y = []
    
    for sample in group:
                
        x += [data_summary['induction_levels'][data_summary['sample'] == sample].values[0]]
        
    xdata = np.linspace(0, 1, 1000)
    
    th, slp, acc_basal = popt[count_group]

    acc[count_group] = acc_func(xdata, th, slp, acc_basal)
    
    cap[count_group] = acc_to_cap_func(acc[count_group], acc_basal)
    
    count_group = count_group + 1


# In[19]:


# Correlation overshooted population is maximal vs induction
data_sets_selected = ['scFv', 'amy']

fig, axs = plt.subplots(len(data_sets_selected), 1)
fig.set_size_inches(8*0.25, 1.7*2, forward=True)
fig.subplots_adjust(hspace = 0.1, wspace=0.2)

count_group = 0

ind_th = []

for data_set in data_sets_selected:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels'])
    group = group_data_sorted['sample']
        
    ax = axs[count_group]
    
    x_all = []
    y_all = []
        
    th, slp, acc_basal = popt[count_group]
    
    xdata = np.linspace(0, 1, 1000)

    capacity_th = 0.05
    
    ind_th += [(capacity_th/slp) + th]

    ax.plot(xdata, cap[count_group],                 '--', color = sm.to_rgba(0.5), linewidth = 1, alpha = 1)
    
    ax.plot([-1, 1], [capacity_th, capacity_th],                 '--', color = 'k', linewidth = 1, alpha = 0.3)
    
    ax.plot([ind_th[count_group], ind_th[count_group]], [-1, 1],                 '--', color = 'cornflowerblue', linewidth = 1, alpha = 0.3)
            
    ax.text(0, 0.25, 'Induction th.: ' + str(round(ind_th[count_group], 2)))

    x = []
    y = []
        
    for sample in group:
        colorVal = sm.to_rgba(data_summary['induction_levels'][data_summary['sample'] == sample].values[0])
            
        x += [data_summary['induction_levels'][data_summary['sample'] == sample].values[0]]
        
        y += [np.max(POI_median[sample]['fraction_accumulators_UPR'][(POI_median[sample]['time_norm'] > 0) & (POI_median[sample]['time_norm'] < 24)]) - acc_basal]
        
        ax.plot(x[-1], y[-1],                      'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = colorVal)
        
    if data_set == data_sets_selected[0]:
        ax.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=True,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off 

    ax.set_xlim(-0.03, 1.03)
    ax.set_ylim(-0.03, 0.3)
    
    count_group = count_group + 1
    
axs[1].set_xlabel('Norm. induction levels')
axs[1].set_ylabel('Maximal fraction of net increase of UPR-defined accumulators')

plt.savefig('acc_opt.svg')


# In[20]:


# Correlation overshooted population is maximal vs induction
data_sets_selected = ['scFv', 'amy']

fig, axs = plt.subplots(len(data_sets_selected), 1)
fig.set_size_inches(8*0.25, 1.7*2, forward=True)
fig.subplots_adjust(hspace = 0.1, wspace=0.2)

count_group = 0

for data_set in data_sets_selected:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels'])
    group = group_data_sorted['sample']
    
    count_sample = 0
    
    ax = axs[count_group]
    
    x_all = []
    y_all = []
    
    for sample in group:
        
        colorVal = sm.to_rgba(data_summary['induction_levels'][data_summary['sample'] == sample].values[0])
        
        x = data_summary['induction_levels'][data_summary['sample'] == sample].values[0]
        
        y = data_summary['sec'][data_summary['sample'] == sample].values[0]
  
        x_all += [x]
        y_all += [y]

        axs[count_group].plot(x, y,                      'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = colorVal)

        count_sample = count_sample + 1
        
    if (data_set == 'scFv') or (data_set == 'amy'):
        
        if data_set == 'scFv':
            degree = 4
        if data_set == 'amy':
            degree = 2
               
    model = np.polyfit([0] + x_all, [0] + y_all, degree) 
    predict = np.poly1d(model)
    x_lin_reg = np.linspace(0, np.max(x_all))
    y_lin_reg = predict(x_lin_reg)

    ax.plot(x_lin_reg, y_lin_reg,                 '--', color = sm.to_rgba(0.5), linewidth = 1, alpha = 1) 
    
    x = ind_th[count_group]
        
    ax.plot([x, x], [-0.01, 2],             '--', color = 'cornflowerblue', linewidth = 1, alpha = 0.3) 
    
    if data_set == data_sets_selected[0]:
        ax.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=True,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off 

    ax.set_xlim(-0.01, 1)
    ax.set_ylim(-0.01, 2)
    
    ax.set_ylabel('Norm. sec. lev. $_{24h}$')
    
    count_group = count_group + 1
    
axs[1].set_xlabel('Norm. induction levels')
    
plt.savefig('sec_opt.svg')


# In[21]:


# Inference of bimodals
# If weight is lower than % use unimodal         
threshold = 5

GM_data_sets = {}

data_sets_G3 = ['scFv', 'amy']

for data_set in data_sets_G3:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    GM_samples = {}
    
    for sample in group:
        
        # Compute AF
           
        GM_df = pd.DataFrame()
        
        POI[sample]['n_cell'] = np.linspace(0,1,len(POI[sample]))
        t_points = POI[sample].groupby(['time_norm'])
        
        GM_points_means_1 = []
        GM_points_means_2 = []
        GM_points_std_1 = []
        GM_points_std_2 = []
        GM_points_time = []
        
        for point in list(range(len(t_points))):

            t_point = t_points.get_group(list(t_points.groups)[point])
            
            df = pd.DataFrame()
            df['X'] = t_point['time_norm']
            df['Y'] = t_point['IF']
            
            # Spherical covariance matrix should vork for 1D data as here
            gmm = GaussianMixture(n_components = 2, covariance_type='spherical')
            gmm.fit(df)
            
            # Constrains in wieght, time (0 - 15h and induction levels (>0.3)                       
            if (gmm.weights_[0] < threshold/100) or (gmm.weights_[0] > (1-(threshold/100)))             or (gmm.means_[0][0] <= 0) or (gmm.means_[0][0] > 15)            or (data_summary['induction_levels'][data_summary['sample'] == sample].values[0] < 0.3):
                
                gmm = GaussianMixture(n_components = 1, covariance_type='spherical')
                gmm.fit(df)
                
                GM_points_means_1.append(gmm.means_[0][1])
                GM_points_means_2.append(gmm.means_[0][1])
                
                # square root of the covariance as the standard deviation
                GM_points_std_1.append(np.sqrt(gmm.covariances_[0]))
                GM_points_std_2.append(np.sqrt(gmm.covariances_[0]))
                
                GM_points_time.append(gmm.means_[0][0])
                               
            else:
                # Separate both populations depending on the value, one low IF another higher
                values = [gmm.means_[0][1], gmm.means_[1][1]]
                idx_min = np.argmin(values)
                idx_max = np.argmax(values)
                GM_points_means_1.append(values[idx_min])
                GM_points_means_2.append(values[idx_max])
                
                # square root of the covariance as the standard deviation
                GM_points_std_1.append(np.sqrt(gmm.covariances_[idx_min]))
                GM_points_std_2.append(np.sqrt(gmm.covariances_[idx_max]))
                
                GM_points_time.append(gmm.means_[0][0])                

        # Smooth data
        
        POI_median[sample]['IF_1'] = GM_points_means_1
        POI_median[sample]['IF_std_1'] = np.array(GM_points_std_1)
        POI_median[sample]['IF_CV_1'] = POI_median[sample]['IF_std_1']/POI_median[sample]['IF_1']
        POI_median[sample]['IF_2'] = GM_points_means_2
        POI_median[sample]['IF_std_2'] = np.array(GM_points_std_2)
        POI_median[sample]['IF_CV_2'] = POI_median[sample]['IF_std_2']/POI_median[sample]['IF_2']
        
# Will be usefull to have the same variables in the rest of proteins, for further data treatment

data_sets_G1_G2 = ['mNeon-non-sec', 'mNeon', 'XylC', 'Xyl2', 'hPON1']

for data_set in data_sets_G1_G2:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    for sample in group:
        
        POI_median[sample]['IF_1'] = POI_median[sample]['IF']
        POI_median[sample]['IF_std_1'] = np.zeros(len(POI_median[sample]['IF']))
        POI_median[sample]['IF_CV_1'] = POI_median[sample]['IF_1']/POI_median[sample]['IF_std_1'] # I compute the CV mean and median are similar for a Gaussian
        POI_median[sample]['IF_2'] = POI_median[sample]['IF']
        POI_median[sample]['IF_std_2'] = np.zeros(len(POI_median[sample]['IF']))
        POI_median[sample]['IF_CV_2'] = POI_median[sample]['IF_2']/POI_median[sample]['IF_std_2'] # I compute the CV mean and median are similar for a Gaussian


# In[22]:


# Get the UPR levels of the different populations

data_sets_G3 = ['scFv', 'amy']

for data_set in data_sets_G3:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    GM_samples = {}
    
    count_sample = 0
        
    for sample in group:
            
        t_points = POI[sample].groupby(['time_norm'])
        
        UPR_1 = []
        FSC_1 = []
        UPR_2 = []
        FSC_2 = []

        for point in list(range(len(t_points))):

            t_point = t_points.get_group(list(t_points.groups)[point])
            
            IF_mean_1 = POI_median[sample]['IF_1'][POI_median[sample]['time_norm'] == list(t_points.groups)[point]]
            IF_std_1 = POI_median[sample]['IF_std_1'][POI_median[sample]['time_norm'] == list(t_points.groups)[point]]
            
            if IF_mean_1.empty:
                IF_mean_1 = np.nan
                IF_std_1 = np.nan
    
            cells_UPR_1 = t_point[(t_point['IF'] < float(IF_mean_1) + float(IF_std_1)) &                                               (t_point['IF'] > float(IF_mean_1) - float(IF_std_1))]
        
            
            UPR_1.append(np.mean(cells_UPR_1['UPR'])) 
            FSC_1.append(np.mean(cells_UPR_1['FSC-HLin']))
        
            IF_mean_2 = POI_median[sample]['IF_2'][POI_median[sample]['time_norm'] == list(t_points.groups)[point]]
            IF_std_2 = POI_median[sample]['IF_std_2'][POI_median[sample]['time_norm'] == list(t_points.groups)[point]]
            
            if IF_mean_2.empty:
                IF_mean_2 = np.nan
                IF_std_2 = np.nan
    
            cells_UPR_2 = t_point[(t_point['IF'] < float(IF_mean_2) + float(IF_std_2)) &                                               (t_point['IF'] > float(IF_mean_2) - float(IF_std_2))]
        
            
            UPR_2.append(np.mean(cells_UPR_2['UPR']))
            FSC_2.append(np.mean(cells_UPR_2['FSC-HLin']))
            
        POI_median[sample]['UPR_1'] = np.array(UPR_1)
        POI_median[sample]['UPR_2'] = np.array(UPR_2)
        POI_median[sample]['FSC_1'] = FSC_1
        POI_median[sample]['FSC_2'] = FSC_2
        
        
# Will be usefull to have the same variables in the rest of proteins, for further data treatment

data_sets_G1_G2 = ['mNeon-non-sec', 'mNeon', 'XylC', 'Xyl2', 'hPON1']

for data_set in data_sets_G1_G2:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    for sample in group:
        
        POI_median[sample]['UPR_1'] = POI_median[sample]['UPR']
        POI_median[sample]['UPR_2'] = POI_median[sample]['UPR']
        POI_median[sample]['FSC_1'] = POI_median[sample]['FSC-HLin']
        POI_median[sample]['FSC_2'] = POI_median[sample]['FSC-HLin']
            


# In[23]:


def model_1(t, y, p, Ind):
    Kprd, Ktrf, Kdil, Kdeg, Ksec, Kunits = p
    Prd, Trf, Sec = y
        
    # Equations variables
    dPrddt = Kprd * Ind - Kprd * Prd
    dTrfdt = Ktrf * Prd - (Kdil + Ksec + Kdeg) * Trf
    dSecdt = Ksec * Kunits * Trf - Kdil * Sec
    
    dydt = dPrddt, dTrfdt, dSecdt
                           
    return dydt 


# In[24]:


# OVER ALL COST FUNCTION

def score_model_1(p_scale):
    
    delta = 0

    # here I prepare the t_eval data as a dataframe
    # I have to add the time zero for the induction to be triggered at that time

    t_eval_list = list(POI_median[sample]['time_norm']) + list(np.linspace(0, fit_end, 50))

    t_eval_dic = {'time_norm': t_eval_list}

    t_eval_df = pd.DataFrame(data=t_eval_dic)

    t_eval_df = t_eval_df.sort_values(by=['time_norm']).reset_index()

    data_eval = t_eval_df

    induction_eval = induction_levels[sample]

    y = simu.simulation(data_eval, induction_eval, p*p_scale, model_std, start, end, variables_0)

    simulation = pd.DataFrame()

    simulation['time_norm'] = y['time'].copy()

    # Here I automate the variable naming
    count_key = 1

    for variable in variables_name:

        simulation[variable] = y[list(y.keys())[count_key]]

        count_key = count_key + 1

    simulation = simulation[simulation['time_norm'].isin(POI_median[sample]['time_norm'])].reset_index()

    if 'Trf' in variables_to_fit:

        # Here I select the data for which I have cytometry time points
        data_IF = POI_median[sample]['IF_1'][(POI_median[sample]['time_norm']>fit_start) &                                             (POI_median[sample]['time_norm']<fit_end)].copy().reset_index()
                
        # Compute cost (normalized)       
        data = data_IF['IF_1'][(simulation['time_norm']>fit_start) & (simulation['time_norm']<fit_end)]/                    np.max(data_IF['IF_1'])
        
        model = simulation['Trf'][(simulation['time_norm']>fit_start) & (simulation['time_norm']<fit_end)]/                    np.max(data_IF['IF_1'])

        delta_sqr_IF = np.square(data - model)

    else:
        delta_sqr_IF = 0
        
    if 'UPR' in variables_to_fit:

        # Here I select the data for which I have cytometry time points
        data_UPR = POI_median[sample]['UPR_1'][(POI_median[sample]['time_norm']>fit_start) &                                             (POI_median[sample]['time_norm']<fit_end)].copy().reset_index()
                
        # Compute cost (normalized)
        model = simulation['UPR'][(simulation['time_norm']>fit_start) &                                             (simulation['time_norm']<fit_end)]/np.max(data_UPR['UPR_1'])
        
        data = data_UPR['UPR_1'][(simulation['time_norm']>fit_start) &                                             (simulation['time_norm']<fit_end)]/np.max(data_UPR['UPR_1'])

        delta_sqr_UPR = np.square(data - model)
        
    else:
        delta_sqr_UPR = 0

    if 'Sec' in variables_to_fit:
        # Here I select the data for which I have secretion time points
        t_sec = POI_sec['time_norm'][POI_sec['sample'] == sample].values[0]

        d = {'time_norm' : [t_sec]}

        modeled_sec = pd.DataFrame(data=d)

        modeled_sec['sec'] = simulation['Sec'][simulation['time_norm'] == t_sec].values[0]

        # Secretion data normalized to the expected secretors fraction
#         data_sec = POI_sec['sec'][POI_sec['sample'] == sample].values[0]/(1 - np.max(POI_median[sample]['fraction_accumulators']))

        # Secretion data NOT normalized to the expected secretors fraction
        data_sec = POI_sec['sec'][data_summary['sample'] == sample].values[0]

         # Compute cost (normalized) 
        model = modeled_sec['sec']/np.nanmax(data_sec) # avoid 0 denominator
        data = data_sec/np.nanmax(data_sec)

        delta_sqr_sec = np.square(data - model)

        if np.isnan(delta_sqr_sec) is True:
            delta_sqr_sec = 0

    else:
        delta_sqr_sec = 0

    msd_sample = np.mean(delta_sqr_IF) + np.mean(delta_sqr_sec) + np.mean(delta_sqr_UPR) 
    delta = delta + msd_sample
    
    return delta


# In[25]:


parameters_initial = {}
mult_parameters = {}
opt_simulation = {}
opt_parameters = {}
data_per_sample = {}


# In[26]:


# Below set the time for fitting and the max duration of the process
fit_start = start
fit_end = 26 # I give more weight to steady state because the priority here is to get growth rate, so only the term in denominator, no care about unidentifiablity in numerator

# Set time max for the fitting algorithm
Time_minutes = 20

# Set the data sets to fit
data_sets_selected = ['mNeon-non-sec']

# Set model to use
model_std = model_1
score_model = score_model_1

# Set vaiables to fit ['IF', 'SL', 'UPR']
variables_to_fit = ['Trf']

# Set the initial value for prameters
Kprd = 1 # 0
Ktrf = 1 # 1
Kdil = 0.4 # 2
Kdeg = 0 # 3
Ksec = 0 # 4
Kunits = 1 # 5

fixed_parameters = [3, 4, 5]

parameters_guess = [Kprd, Ktrf, Kdil, Kdeg, Ksec, Kunits] 

parameters_name = ['Kprd', 'Ktrf', 'Kdil', 'Kdeg', 'Ksec', 'Kunits']

# Set the initial value of the variables
Prd  = 0
Trf = 0
Sec = 0

variables_0 = [Prd, Trf, Sec] # Chose initial conditions for the simulation

variables_name = ['Prd', 'Trf', 'Sec']

# set if bound parameters is needed

lb = [0] * len(parameters_name)
ub = [None] * len(parameters_name)

################################################################################################

# Here just make the data_sets a list in case it is not
if type(data_sets_selected) is not list:
    data_sets_selected = [data_sets_selected]

for data_set in data_sets_selected:
    
    parameters_first = parameters_guess
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels'], ascending = False)
    group = group_data_sorted['sample']
    
    for sample in group:
            
        opts = dict( bounds=[lb, ub],
        fixed_variables = {k:1 for k in fixed_parameters},
        timeout = Time_minutes * 60)
                        
        parameters_initial[sample] = parameters_first
        p = np.array(parameters_initial[sample])
        
        print('sample: ' + data_summary['POI'][data_summary['sample'] == sample].values[0] + ' ' + str(sample))
        es = cma.CMAEvolutionStrategy(np.ones(p.shape), 1/6, opts)
        res = es.optimize(score_model)
        mult_parameters = es.best.x

        if data_summary['DC'][data_summary['sample'] == sample].values[0] != 0:

            # Simulate the dataset with the las optimized parameters

            opt_parameters[sample] = parameters_initial[sample] * mult_parameters

            # Simulate the systema behaviour with the optimal parameters

            parameters_initial[sample] = opt_parameters[sample]

            # here I prepare the t_eval data as a dataframe
            # I have to add the time zero for the induction to be triggered at that time

            t_eval_list = list(POI_median[sample]['time_norm']) + list(np.linspace(0, fit_end, 50))

            t_eval_dic = {'time_norm': t_eval_list}

            t_eval_df = pd.DataFrame(data=t_eval_dic)

            t_eval_df = t_eval_df.sort_values(by=['time_norm']).reset_index()

            data_eval = t_eval_df

            opt_simulation[sample] = simu.simulation(data_eval, induction_levels[sample],                                                         opt_parameters[sample], model_std, start, end, variables_0)
        
        # Here I set to NaN the parameters for DC = 0
        
        if data_summary['DC'][data_summary['sample'] == sample].values[0] == 0:
            
            a = np.empty(len(parameters_name))

            a[:] = np.nan
        
            opt_parameters[sample] = a 
            
            parameters_initial[sample] = parameters_guess
            
        # Here I just make a dataframe to summarize parameter values
            
        data_per_sample[data_summary['POI'][data_summary['sample'] == sample].values[0] + '_sample_' + str(sample)] = []

        data_per_sample[data_summary['POI'][data_summary['sample'] == sample].values[0] + '_sample_' + str(sample)] =                  data_per_sample[data_summary['POI'][data_summary['sample'] == sample].values[0] + '_sample_' + str(sample)] +                 list(opt_parameters[sample]) +                 [data_summary['induction_levels'][data_summary['sample'] == sample].values[0],                  data_summary['sample'][data_summary['sample'] == sample].values[0], data_summary['POI'][data_summary['sample'] == sample].values[0]]       
        
parameters_opt = pd.DataFrame.from_dict(data_per_sample, orient='index',                                         columns=parameters_name + ['induction_levels'] + ['sample'] + ['POI'])

parameters_opt


# In[27]:


median = np.nanmedian(parameters_opt['Kdil'][parameters_opt['POI'] == 'mNeon-non-sec'])
print('median Kdil = ' + str(round(median, 2)))
Kdil = median

median = np.nanmedian(parameters_opt['Kprd'][parameters_opt['POI'] == 'mNeon-non-sec'])
print('median Kprd = ' + str(round(median, 2)))
Kprd = median


# In[28]:


# Below set the time for fitting and the max duration of the process
fit_start = start
fit_end = 26 # I give more weight to steady state because the priority here is to get growth rate, so only the term in denominator, no care about unidentifiablity in numerator

# Set time max for the fitting algorithm
Time_minutes = 20

# Set the data sets to fit
data_sets_selected = ['mNeon-non-sec']

# Set model to use
model_std = model_1
score_model = score_model_1

# Set vaiables to fit ['IF', 'SL', 'UPR']
variables_to_fit = ['Trf']

# Set the initial value for prameters
Kprd = Kprd # 0
Ktrf = 1 # 1
Kdil = Kdil # 2
Kdeg = 0 # 3
Ksec = 0 # 4
Kunits = 1 # 5

fixed_parameters = [0, 2, 3, 4, 5]

parameters_guess = [Kprd, Ktrf, Kdil, Kdeg, Ksec, Kunits] 

parameters_name = ['Kprd', 'Ktrf', 'Kdil', 'Kdeg', 'Ksec', 'Kunits']

# Set the initial value of the variables
Prd  = 0
Trf = 0
Sec = 0

variables_0 = [Prd, Trf, Sec] # Chose initial conditions for the simulation

variables_name = ['Prd', 'Trf', 'Sec']

# set if bound parameters is needed

lb = [0] * len(parameters_name)
ub = [None] * len(parameters_name)

################################################################################################

# Here just make the data_sets a list in case it is not
if type(data_sets_selected) is not list:
    data_sets_selected = [data_sets_selected]

for data_set in data_sets_selected:
    
    parameters_first = parameters_guess
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels'], ascending = False)
    group = group_data_sorted['sample']
    
    for sample in group:
            
        opts = dict( bounds=[lb, ub],
        fixed_variables = {k:1 for k in fixed_parameters},
        timeout = Time_minutes * 60)
                        
        parameters_initial[sample] = parameters_first
        p = np.array(parameters_initial[sample])
        
        print('sample: ' + data_summary['POI'][data_summary['sample'] == sample].values[0] + ' ' + str(sample))
        es = cma.CMAEvolutionStrategy(np.ones(p.shape), 1/6, opts)
        res = es.optimize(score_model)
        mult_parameters = es.best.x

        if data_summary['DC'][data_summary['sample'] == sample].values[0] != 0:

            # Simulate the dataset with the las optimized parameters

            opt_parameters[sample] = parameters_initial[sample] * mult_parameters

            # Simulate the systema behaviour with the optimal parameters

            parameters_initial[sample] = opt_parameters[sample]

            # here I prepare the t_eval data as a dataframe
            # I have to add the time zero for the induction to be triggered at that time

            t_eval_list = list(POI_median[sample]['time_norm']) + list(np.linspace(0, fit_end, 50))

            t_eval_dic = {'time_norm': t_eval_list}

            t_eval_df = pd.DataFrame(data=t_eval_dic)

            t_eval_df = t_eval_df.sort_values(by=['time_norm']).reset_index()

            data_eval = t_eval_df

            opt_simulation[sample] = simu.simulation(data_eval, induction_levels[sample],                                                         opt_parameters[sample], model_std, start, end, variables_0)
        
        # Here I set to NaN the parameters for DC = 0
        
        if data_summary['DC'][data_summary['sample'] == sample].values[0] == 0:
            
            a = np.empty(len(parameters_name))

            a[:] = np.nan
        
            opt_parameters[sample] = a 
            
            parameters_initial[sample] = parameters_guess
            
        # Here I just make a dataframe to summarize parameter values
            
        data_per_sample[data_summary['POI'][data_summary['sample'] == sample].values[0] + '_sample_' + str(sample)] = []

        data_per_sample[data_summary['POI'][data_summary['sample'] == sample].values[0] + '_sample_' + str(sample)] =                  data_per_sample[data_summary['POI'][data_summary['sample'] == sample].values[0] + '_sample_' + str(sample)] +                 list(opt_parameters[sample]) +                 [data_summary['induction_levels'][data_summary['sample'] == sample].values[0],                  data_summary['sample'][data_summary['sample'] == sample].values[0], data_summary['POI'][data_summary['sample'] == sample].values[0]]       
        
parameters_opt = pd.DataFrame.from_dict(data_per_sample, orient='index',                                         columns=parameters_name + ['induction_levels'] + ['sample'] + ['POI'])

parameters_opt


# In[29]:


median = np.nanmedian(parameters_opt['Kprd'][parameters_opt['POI'] == 'mNeon-non-sec'])
print('median Kprd = ' + str(round(median, 2)))

Kprd = median


# In[30]:


median = np.nanmedian(parameters_opt['Ktrf'][parameters_opt['POI'] == 'mNeon-non-sec'])
print('median Ktrf = ' + str(round(median, 2)))

Ktrf = median


# In[31]:


# Below set the time for fitting and the max duration of the process
fit_start = start
fit_end = 26 # I give more weight to steady state because the priority here is to get growth rate, so only the term in denominator, no care about unidentifiablity in numerator

# Set time max for the fitting algorithm
Time_minutes = 20

# Set the data sets to fit
data_sets_selected = ['mNeon']

# Set model to use
model_std = model_1
score_model = score_model_1

# Set vaiables to fit ['IF', 'SL', 'UPR']
variables_to_fit = ['Trf', 'Sec']

# Set the initial value for prameters
Kprd = Kprd # 0
Ktrf = Ktrf # 1
Kdil = Kdil # 2
Kdeg = 0 # 3
Ksec = 1 # 4
Kunits = 1000 # 5

fixed_parameters = [0, 2, 3]

parameters_guess = [Kprd, Ktrf, Kdil, Kdeg, Ksec, Kunits] 

parameters_name = ['Kprd', 'Ktrf', 'Kdil', 'Kdeg', 'Ksec', 'Kunits']

# Set the initial value of the variables
Prd  = 0
Trf = 0
Sec = 0

variables_0 = [Prd, Trf, Sec] # Chose initial conditions for the simulation

variables_name = ['Prd', 'Trf', 'Sec']

# set if bound parameters is needed

lb = [0] * len(parameters_name)
ub = [None] * len(parameters_name)

################################################################################################

# Here just make the data_sets a list in case it is not
if type(data_sets_selected) is not list:
    data_sets_selected = [data_sets_selected]

for data_set in data_sets_selected:
    
    parameters_first = parameters_guess
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels'], ascending = False)
    group = group_data_sorted['sample']
    
    for sample in group:
            
        opts = dict( bounds=[lb, ub],
        fixed_variables = {k:1 for k in fixed_parameters},
        timeout = Time_minutes * 60)
                        
        parameters_initial[sample] = parameters_first
        p = np.array(parameters_initial[sample])
        
        print('sample: ' + data_summary['POI'][data_summary['sample'] == sample].values[0] + ' ' + str(sample))
        es = cma.CMAEvolutionStrategy(np.ones(p.shape), 1/6, opts)
        res = es.optimize(score_model)
        mult_parameters = es.best.x
        
        if data_summary['DC'][data_summary['sample'] == sample].values[0] != 0:

            # Simulate the dataset with the las optimized parameters

            opt_parameters[sample] = parameters_initial[sample] * mult_parameters

            # Simulate the systema behaviour with the optimal parameters

            parameters_initial[sample] = opt_parameters[sample]

            # here I prepare the t_eval data as a dataframe
            # I have to add the time zero for the induction to be triggered at that time

            t_eval_list = list(POI_median[sample]['time_norm']) + list(np.linspace(0, fit_end, 50))

            t_eval_dic = {'time_norm': t_eval_list}

            t_eval_df = pd.DataFrame(data=t_eval_dic)

            t_eval_df = t_eval_df.sort_values(by=['time_norm']).reset_index()

            data_eval = t_eval_df

            opt_simulation[sample] = simu.simulation(data_eval, induction_levels[sample],                                                         opt_parameters[sample], model_std, start, end, variables_0)
        
        # Here I set to NaN the parameters for DC = 0
        
        if data_summary['DC'][data_summary['sample'] == sample].values[0] == 0:
            
            a = np.empty(len(parameters_name))

            a[:] = np.nan
        
            opt_parameters[sample] = a 
            
            parameters_initial[sample] = parameters_guess
            
        # Here I just make a dataframe to summarize parameter values
            
        data_per_sample[data_summary['POI'][data_summary['sample'] == sample].values[0] + '_sample_' + str(sample)] = []

        data_per_sample[data_summary['POI'][data_summary['sample'] == sample].values[0] + '_sample_' + str(sample)] =                  data_per_sample[data_summary['POI'][data_summary['sample'] == sample].values[0] + '_sample_' + str(sample)] +                 list(opt_parameters[sample]) +                 [data_summary['induction_levels'][data_summary['sample'] == sample].values[0],                  data_summary['sample'][data_summary['sample'] == sample].values[0], data_summary['POI'][data_summary['sample'] == sample].values[0]]       
        
parameters_opt = pd.DataFrame.from_dict(data_per_sample, orient='index',                                         columns=parameters_name + ['induction_levels'] + ['sample'] + ['POI'])

parameters_opt


# In[32]:


median = np.nanmedian(parameters_opt['Kunits'][parameters_opt['POI'] == 'mNeon'])
print('median Kunits = ' + str(round(median, 2)))
Kunits = median


# In[33]:


# Below set the time for fitting and the max duration of the process
fit_start = start
fit_end = 26 # I give more weight to steady state because the priority here is to get growth rate, so only the term in denominator, no care about unidentifiablity in numerator

# Set time max for the fitting algorithm
Time_minutes = 20

# Set the data sets to fit
data_sets_selected = ['mNeon']

# Set model to use
model_std = model_1
score_model = score_model_1

# Set vaiables to fit ['IF', 'SL', 'UPR']
variables_to_fit = ['Trf', 'Sec']

fixed_parameters = [0, 2, 3, 5]

parameters_guess = [Kprd, Ktrf, Kdil, Kdeg, Ksec, Kunits] 

parameters_name = ['Kprd', 'Ktrf', 'Kdil', 'Kdeg', 'Ksec', 'Kunits']

# Set the initial value of the variables
Prd  = 0
Trf = 0
Sec = 0

variables_0 = [Prd, Trf, Sec] # Chose initial conditions for the simulation

variables_name = ['Prd', 'Trf', 'Sec']

# set if bound parameters is needed

lb = [0] * len(parameters_name)
ub = [None] * len(parameters_name)

################################################################################################

# Here just make the data_sets a list in case it is not
if type(data_sets_selected) is not list:
    data_sets_selected = [data_sets_selected]

for data_set in data_sets_selected:
    
    parameters_first = parameters_guess
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels'], ascending = False)
    group = group_data_sorted['sample']
    
    for sample in group:
            
        opts = dict( bounds=[lb, ub],
        fixed_variables = {k:1 for k in fixed_parameters},
        timeout = Time_minutes * 60)
        
        # IN THIS SPECIFIC CASE I USE THE PREVIOUS PARAMETERS
        
        if data_summary['DC'][data_summary['sample'] == sample].values[0] != 0:
        
            parameters_first = opt_parameters[sample]
            parameters_first[5] = Kunits
                        
        parameters_initial[sample] = parameters_first
        p = np.array(parameters_initial[sample])
        
        print('sample: ' + data_summary['POI'][data_summary['sample'] == sample].values[0] + ' ' + str(sample))
        es = cma.CMAEvolutionStrategy(np.ones(p.shape), 1/6, opts)
        res = es.optimize(score_model)
        mult_parameters = es.best.x

        if data_summary['DC'][data_summary['sample'] == sample].values[0] != 0:

            # Simulate the dataset with the las optimized parameters

            opt_parameters[sample] = parameters_initial[sample] * mult_parameters

            # Simulate the systema behaviour with the optimal parameters

            parameters_initial[sample] = opt_parameters[sample]

            # here I prepare the t_eval data as a dataframe
            # I have to add the time zero for the induction to be triggered at that time

            t_eval_list = list(POI_median[sample]['time_norm']) + list(np.linspace(0, fit_end, 50))

            t_eval_dic = {'time_norm': t_eval_list}

            t_eval_df = pd.DataFrame(data=t_eval_dic)

            t_eval_df = t_eval_df.sort_values(by=['time_norm']).reset_index()

            data_eval = t_eval_df

            opt_simulation[sample] = simu.simulation(data_eval, induction_levels[sample],                                                         opt_parameters[sample], model_std, start, end, variables_0)
        
        # Here I set to NaN the parameters for DC = 0
        
        if data_summary['DC'][data_summary['sample'] == sample].values[0] == 0:
            
            a = np.empty(len(parameters_name))

            a[:] = np.nan
        
            opt_parameters[sample] = a 
            
            parameters_initial[sample] = parameters_guess
            
        # Here I just make a dataframe to summarize parameter values
            
        data_per_sample[data_summary['POI'][data_summary['sample'] == sample].values[0] + '_sample_' + str(sample)] = []

        data_per_sample[data_summary['POI'][data_summary['sample'] == sample].values[0] + '_sample_' + str(sample)] =                  data_per_sample[data_summary['POI'][data_summary['sample'] == sample].values[0] + '_sample_' + str(sample)] +                 list(opt_parameters[sample]) +                 [data_summary['induction_levels'][data_summary['sample'] == sample].values[0],                  data_summary['sample'][data_summary['sample'] == sample].values[0], data_summary['POI'][data_summary['sample'] == sample].values[0]]       
        
parameters_opt = pd.DataFrame.from_dict(data_per_sample, orient='index',                                         columns=parameters_name + ['induction_levels'] + ['sample'] + ['POI'])

parameters_opt


# In[34]:


median = np.nanmedian(parameters_opt['Kprd'][parameters_opt['POI'] == 'mNeon'])
print('median Kprd = ' + str(round(median, 2)))
Kprd = median

median = np.nanmedian(parameters_opt['Ktrf'][parameters_opt['POI'] == 'mNeon'])
print('median Ktrf = ' + str(round(median, 2)))
Ktrf = median

median = np.nanmedian(parameters_opt['Kdil'][parameters_opt['POI'] == 'mNeon'])
print('median Kdil = ' + str(round(median, 2)))
Kdil = median

median = np.nanmedian(parameters_opt['Ksec'][parameters_opt['POI'] == 'mNeon'])
print('median Ksec = ' + str(round(median, 2)))
Ksec = median

median = np.nanmedian(parameters_opt['Kunits'][parameters_opt['POI'] == 'mNeon'])
print('median Kunits = ' + str(round(median, 2)))
Kunits = median


# In[35]:


# Below set the time for fitting and the max duration of the process
fit_start = start
fit_end = 10 # I give more weight to steady state because the priority here is to get growth rate, so only the term in denominator, no care about unidentifiablity in numerator

# Set time max for the fitting algorithm
Time_minutes = 20

# Set the data sets to fit
data_sets_selected = ['scFv', 'amy']   

# Set model to use
model_std = model_1
score_model = score_model_1

# Set vaiables to fit ['IF', 'SL', 'UPR']
variables_to_fit = ['Trf', 'Sec']

# Set the initial value for prameters
Kprd = Kprd # 0
Ktrf = Ktrf # 1
Kdil = Kdil # 2
Kdeg = 1 # 3
Ksec = Ksec # 4
Kunits = Kunits # 5

fixed_parameters = [0, 2, 5]

parameters_guess = [Kprd, Ktrf, Kdil, Kdeg, Ksec, Kunits] 

parameters_name = ['Kprd', 'Ktrf', 'Kdil', 'Kdeg', 'Ksec', 'Kunits']

# Set the initial value of the variables
Prd  = 0
Trf = 0
Sec = 0

variables_0 = [Prd, Trf, Sec] # Chose initial conditions for the simulation

variables_name = ['Prd', 'Trf', 'Sec']

# set if bound parameters is needed

lb = [0] * len(parameters_name)
ub = [None] * len(parameters_name)

# lb[1] = 0
# ub[1] = 1

################################################################################################

# Here just make the data_sets a list in case it is not
if type(data_sets_selected) is not list:
    data_sets_selected = [data_sets_selected]

for data_set in data_sets_selected:
    
    parameters_first = parameters_guess
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels'], ascending = False)
    group = group_data_sorted['sample']
    
    for sample in group:
            
        opts = dict( bounds=[lb, ub],
        fixed_variables = {k:1 for k in fixed_parameters},
        timeout = Time_minutes * 60)
                        
        parameters_initial[sample] = parameters_first
        p = np.array(parameters_initial[sample])
        
        print('sample: ' + data_summary['POI'][data_summary['sample'] == sample].values[0] + ' ' + str(sample))
        es = cma.CMAEvolutionStrategy(np.ones(p.shape), 1/6, opts)
        res = es.optimize(score_model)
        mult_parameters = es.best.x

        if data_summary['DC'][data_summary['sample'] == sample].values[0] != 0:

            # Simulate the dataset with the las optimized parameters

            opt_parameters[sample] = parameters_initial[sample] * mult_parameters

            # Simulate the systema behaviour with the optimal parameters

            parameters_initial[sample] = opt_parameters[sample]

            # here I prepare the t_eval data as a dataframe
            # I have to add the time zero for the induction to be triggered at that time

            t_eval_list = list(POI_median[sample]['time_norm']) + list(np.linspace(0, fit_end, 50))

            t_eval_dic = {'time_norm': t_eval_list}

            t_eval_df = pd.DataFrame(data=t_eval_dic)

            t_eval_df = t_eval_df.sort_values(by=['time_norm']).reset_index()

            data_eval = t_eval_df

            opt_simulation[sample] = simu.simulation(data_eval, induction_levels[sample],                                                         opt_parameters[sample], model_std, start, end, variables_0)
        
        # Here I set to NaN the parameters for DC = 0
        
        if data_summary['DC'][data_summary['sample'] == sample].values[0] == 0:
            
            a = np.empty(len(parameters_name))

            a[:] = np.nan
        
            opt_parameters[sample] = a 
            
            parameters_initial[sample] = parameters_guess
            
        # Here I just make a dataframe to summarize parameter values
            
        data_per_sample[data_summary['POI'][data_summary['sample'] == sample].values[0] + '_sample_' + str(sample)] = []

        data_per_sample[data_summary['POI'][data_summary['sample'] == sample].values[0] + '_sample_' + str(sample)] =                  data_per_sample[data_summary['POI'][data_summary['sample'] == sample].values[0] + '_sample_' + str(sample)] +                 list(opt_parameters[sample]) +                 [data_summary['induction_levels'][data_summary['sample'] == sample].values[0],                  data_summary['sample'][data_summary['sample'] == sample].values[0], data_summary['POI'][data_summary['sample'] == sample].values[0]]       
        
parameters_opt = pd.DataFrame.from_dict(data_per_sample, orient='index',                                         columns=parameters_name + ['induction_levels'] + ['sample'] + ['POI'])

parameters_opt


# In[36]:


# open file for writing
dict_temp = {}

for sample in list(opt_parameters.keys()):
    dict_temp[sample] = list(opt_parameters[sample])


with open('opt_parameters_G2_1.json', 'w') as fp:
    json.dump(dict_temp, fp)


# In[37]:


# This is commented, but you can use it to plot the fits

# A function to plot the data
def plot_summary_model_1(data_set, time_axis):
    
    group = data_summary['sample'][data_summary['POI'] == data_set]

    fig, axs = plt.subplots(1, 1)
    fig.set_size_inches(8*0.9/8, 1.7/1.5, forward=True)
    fig.subplots_adjust(hspace = 0, wspace=0)

    # colormap
    N = len(induction_levels)
    cmap = plt.get_cmap('Blues', N)

    # Normalizer
    import matplotlib as mpl
    norm = mpl.colors.Normalize(vmin=np.min(data_summary['induction_levels']),                      vmax=np.max(data_summary['induction_levels']))

    # creating ScalarMappable
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    
    UPR_y_upperlim = np.nanmax(data_summary['UPR_ss'][data_summary['POI'] == data_set]) * 1.2

    for sample in group:
        
        if sample in list(opt_simulation.keys()):
        
            colorVal = sm.to_rgba(data_summary['induction_levels'][data_summary['sample'] == sample].values[0])

            if data_summary['replica'][data_summary['sample'] == sample].values[0] == 1:
                marker = 'o'
            if data_summary['replica'][data_summary['sample'] == sample].values[0] == 2:
                marker = 's'
            if data_summary['replica'][data_summary['sample'] == sample].values[0] == 3:
                marker = '^'

            y = opt_simulation[sample]

            # Here I automate the variable naming
            simulation = pd.DataFrame()

            count_key = 1

            for variable in variables_name:

                simulation[variable] = y[list(y.keys())[count_key]]

                count_key = count_key + 1

            simulation['time_norm'] = opt_simulation[sample]['time']

            # Plot the IF only if sample is even number
            
            if (sample % 2) == 0:
    
                plt.plot(POI_median[sample]['time_norm'], POI_median[sample]['IF_1'],                            marker, MarkerSize = 2, color = colorVal, markeredgecolor='k', markeredgewidth=0.2)

                plt.xlabel('Time (h)')
                plt.ylabel('Trf or iPOI (RPU)')
                plt.xlim(time_axis)

                if 'Trf' in variables_to_fit:

                    simulation['Trf']  = simulation['Trf']

                    x = simulation['time_norm']
                    y = simulation['Trf'] 


                    plt.plot(x, y,                                '-', LineWidth = 0.5, color = colorVal)

                    plt.plot(x, y,                                 '--', color = 'k', LineWidth = 0.1)

    return axs


data_sets_selected = ['scFv', 'amy']

time_axis = [0,10]

for data_set in data_sets_selected:
    ax = plot_summary_model_1(data_set, time_axis)
    
    plt.savefig('fits_' + data_set + '.svg')


# In[38]:


params = {'legend.fontsize': 6,
          'legend.loc' :'lower right',
          'legend.markerscale'   : 1,
          'axes.labelsize' : 6,
          'xtick.labelsize' : 6,
          'ytick.labelsize' : 6,
          'axes.titlesize' : 6,

          'font.family':  'Calibri',
          'font.style':   'normal',
          'font.variant': 'normal',
          'font.weight':  100,
          'font.stretch': 'normal',
          'font.size':    6.0,
          }

plt.rcParams.update(params)


# In[39]:


# Correlation overshooted population is maximal vs induction
data_sets_selected = ['scFv', 'amy']

fig, axs = plt.subplots(len(data_sets_selected), 1)
fig.set_size_inches(8*0.07, 1.7*0.7, forward=True)
fig.subplots_adjust(hspace = 0.1, wspace=0.2)

count_group = 0

for data_set in data_sets_selected:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels'])
    group = group_data_sorted['sample']
        
    x_real = []
    y_trf = []
    
    for sample in group:
        
        if data_summary['DC'][data_summary['sample'] == sample].values[0] > 0:     
            
            colorVal = sm.to_rgba(data_summary['induction_levels'][data_summary['sample'] == sample].values[0])
            
            x_t = data_summary['induction_levels'][data_summary['sample'] == sample].values[0]
                        
            y_trf_t = parameters_opt['Ktrf'][parameters_opt['sample'] == sample].values[0] 
            x_real += [x_t]
            y_trf += [y_trf_t]

# ----------------------------------------------------------------------

    ax=axs[count_group]

    y_smooth_trf = savgol_filter(y_trf, 3, 1)
    y = y_smooth_trf
    ax.plot(x_real, y,                     'o', MarkerSize = 3, markeredgecolor = 'k', markeredgewidth = 0.2 , color = 'cornflowerblue') 
    
    model = np.polyfit(x_real, y, 5)
    predict = np.poly1d(model)
    x_lin_reg = np.linspace(np.min(x_real), np.max(x_real), 100)
    y_lin_reg = predict(x_lin_reg)
    y = y_lin_reg
    x = x_lin_reg
    ax.plot(x, y, '--', color = 'grey',             LineWidth = 2, alpha = 0.2)
    
    ax.set_ylabel('Ktrf')
    
    ax.set_ylim(0, np.max(y_smooth_trf) * 1.2) 
    
    ax.yaxis.set_major_locator(MaxNLocator(3)) 

    axs[count_group].set_xlim(-0.01, 1)
    
    axs[count_group].set_ylim(0, np.max(y)*1.2)
                
    count_group = count_group + 1

plt.savefig('parameters_Ktrf.svg')


# In[40]:


params = {'legend.fontsize': 8,
          'legend.loc' :'lower right',
          'legend.markerscale'   : 1,
          'axes.labelsize' : 8,
          'xtick.labelsize' : 8,
          'ytick.labelsize' : 8,
          'axes.titlesize' : 8,

          'font.family':  'Calibri',
          'font.style':   'normal',
          'font.variant': 'normal',
          'font.weight':  100,
          'font.stretch': 'normal',
          'font.size':    8.0,
          }

plt.rcParams.update(params)


# In[41]:


# Correlation overshooted population is maximal vs induction
data_sets_selected = ['scFv', 'amy']

fig, axs = plt.subplots(len(data_sets_selected), 1)
fig.set_size_inches(8*0.25, 1.7*2, forward=True)
fig.subplots_adjust(hspace = 0.1, wspace=0.2)

count_group = 0

for data_set in data_sets_selected:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['induction_levels'])
    group = group_data_sorted['sample']
        
    x_real = []
    y_deg = []
    y_sec = []
    
    for sample in group:
        
        if data_summary['DC'][data_summary['sample'] == sample].values[0] > 0:     
            
            colorVal = sm.to_rgba(data_summary['induction_levels'][data_summary['sample'] == sample].values[0])
            
            x_t = data_summary['induction_levels'][data_summary['sample'] == sample].values[0]
            
            y_deg_t = parameters_opt['Kdeg'][parameters_opt['sample'] == sample].values[0]             
            y_sec_t = parameters_opt['Ksec'][parameters_opt['sample'] == sample].values[0] 
            x_real += [x_t]
            y_deg += [y_deg_t]
            y_sec += [y_sec_t]

# ----------------------------------------------------------------------

    ax_sec=axs[count_group]

    y_smooth_sec = savgol_filter(y_sec, 3, 1)
    y = y_smooth_sec
    ax_sec.plot(x_real, y,                     'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = 'palegreen', label = 'sec') 
    
    model = np.polyfit(x_real, y, 5)
    predict = np.poly1d(model)
    x_lin_reg = np.linspace(np.min(x_real), np.max(x_real), 100)
    y_lin_reg = predict(x_lin_reg)
    y = y_lin_reg
    x = x_lin_reg
    ax_sec.plot(x, y, '--', color = 'g',             LineWidth = 2, alpha = 0.2, label = 'sec')
    
    ax_sec.set_ylabel('Ksec', color = 'g')
    ax_sec.set_ylim(0, np.max(y_smooth_sec) * 1.2)
    
# ----------------------------------------------------------------------

    ax_deg = ax_sec.twinx()
    
    y_smooth_deg = savgol_filter(y_deg, 3, 1)
    y = y_smooth_deg
    ax_deg.plot(x_real, y,                     'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = 'salmon', label = 'deg')
    
    model = np.polyfit(x_real, y, 5)  
    predict = np.poly1d(model)
    x_lin_reg = np.linspace(np.min(x_real), np.max(x_real), 100)
    y_lin_reg = predict(x_lin_reg)
    y = y_lin_reg
    x = x_lin_reg
    ax_deg.plot(x, y, '--', color = 'r',             LineWidth = 2, alpha = 0.2, label = 'deg')
    
    ax_deg.set_ylabel('Kdeg', color = 'r')
    ax_deg.set_ylim(0, np.max(y_smooth_deg) * 1.2)
    
# ----------------------------------------------------------------------
    
    axs[count_group].set_xlim(-0.01, 1)
    
    ax_deg.plot([ind_th[count_group], ind_th[count_group]], [0, np.max(y_smooth_deg) * 1.2],             '--', color = 'cornflowerblue', linewidth = 1, alpha = 0.3) 
    
#     ax_deg.text(0, np.max(y_smooth_deg) ,data_set)

    if data_set == data_sets_selected[0]:
            axs[count_group].tick_params(
            axis='x',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            bottom=True,      # ticks along the bottom edge are off
            top=False,         # ticks along the top edge are off
            labelbottom=False) # labels along the bottom edge are off 
            
    ax_sec.yaxis.set_major_locator(MaxNLocator(4)) 

    count_group = count_group + 1
    
axs[-1].set_xlabel('Norm. induction levels')

plt.savefig('parameters.svg')


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




