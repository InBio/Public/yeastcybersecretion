#!/usr/bin/env python
# coding: utf-8

# ## Import modules and select data

# In[1]:


# Python modules
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.ticker import MaxNLocator
from matplotlib.offsetbox import AnchoredText
from matplotlib.ticker import FormatStrFormatter
from joypy import joyplot
from scipy.signal import savgol_filter
from scipy.optimize import curve_fit
import sys

# Main path
path = 'C:\\Users\\lifeware\\Desktop\\paper'

# In-house modules
sys.path.append(path + '\\code')

import getting_processed_data
import Parsing
import simu


# In[2]:


# IMPORT and PARSE data
# Write the experiment date in the order that you want them

scFv = [20210817, 20211115]

FB_1 = [20211117]

FB_2 = [20211124]

FB_3 = [20220125]

FB_5 = [20220510]

experiment_dates = scFv + FB_1 + FB_2 + FB_3 + FB_5

# Write the experiment (secretion measurement) date in the order that you want them (same as above)

scFv = [20210819, 20211117]

FB_1 = [20211119]

FB_2 = [20211126]

FB_3 = [20220127]

FB_5 = [202205122]

sec_experiment_dates = scFv + FB_1 + FB_2 + FB_3 + FB_5

# Secretion events threshold, below that, the secretion levels would be NaN
events_th = 10

# Order the data sets as you like
data_sets_charact = ['scFv']   

data_sets = ['scFv', 'FB_1', 'FB_2', 'FB_3', 'FB_5']   

# if want to discard some sample, make a list of tupples for each (data set, replica, reactor), or enter ('None')

outliers = [('None')]

# Set an approximate time length of the experiment (the LED is on at time 0):
start = -3
end = 26


# In[3]:


# Here I get the data sets, tell if you use sensor strain
LED, cells_raw, cells_corrected, cells, POI, POI_median, metadata, sensor, sensor_median =         getting_processed_data.get_processed_data(experiment_dates, path, outliers)


# In[4]:


# Here I get the data sets only for secretion
sec_raw, sec = Parsing.sec_parse(experiment_dates, sec_experiment_dates, metadata, POI_median, path, events_th)


# In[5]:


data_summary = metadata.copy()
        
# Here I compute steady state values of x[variable] between t_start and t_end
x = POI_median
variable = 'IF'
t_end = 24
t_start = t_end - 8
IF_ss = Parsing.steady_states(x, variable, t_start, t_end, metadata)

x = POI_median
variable = 'UPR'
UPR_ss = Parsing.steady_states(x, variable, t_start, t_end, metadata)

data_summary['IF_ss'] = IF_ss.copy()
data_summary['UPR_ss'] = UPR_ss.copy()

POI_sec = pd.DataFrame()
 
POI_sec['sec'] = list(sec.values())

POI_sec['sample'] = list(data_summary['sample'])

# and then use the proteomics data to scale it based on the experiment with same duty cycle

proteomics = pd.read_csv (path + '\\data\\proteomics_metric.csv')

for data_set in data_sets:
    
    sample = data_summary[(data_summary['POI'] == data_set) &                           (data_summary['DC'] == 1)].sort_values(['DC']).iloc[-1]['sample']
    
    OM_factor = 10**proteomics['OM'][proteomics['POI'] == 'scFv'].values[0]/POI_sec['sec'][POI_sec['sample'] == sample].values[0]   
    
    for sample in data_summary[data_summary['POI'] == data_set]['sample']:
        
        POI_sec['sec'].loc[POI_sec['sample'] == sample] = POI_sec['sec'].loc[POI_sec['sample'] == sample] * OM_factor
    
data_summary['sec'] = POI_sec['sec'].copy()
        
data_summary


# In[6]:


# Here I substract the fluorescence of secretion at induction 0, I count it as a blank 
    
group_0 = data_summary['sample'][data_summary['DC'] == 0]

for sample in group_0:

    blank = data_summary['sec'][data_summary['sample'] == sample].values[0]
    
    if np.isnan(blank) == False:
    
        replica = data_summary['replica'][data_summary['sample'] == sample].values[0]

        group = data_summary['sample'][data_summary['POI'] == data_summary['POI'][data_summary['sample'] == sample].values[0]]

        for sample_of_replica in group:

            if data_summary['replica'][data_summary['sample'] == sample_of_replica].values[0] == replica:

                data_summary['sec'].loc[data_summary['sample'] == sample_of_replica] =                 data_summary['sec'][data_summary['sample'] == sample_of_replica].copy() - blank

                POI_sec['sec'].loc[POI_sec['sample'] == sample_of_replica] =                 data_summary['sec'][data_summary['sample'] == sample_of_replica].copy()


# In[7]:


params = {'legend.fontsize': 8,
          'legend.loc' :'lower right',
          'legend.markerscale'   : 1,
          'axes.labelsize' : 8,
          'xtick.labelsize' : 8,
          'ytick.labelsize' : 8,
          'axes.titlesize' : 8,

          'font.family':  'Calibri',
          'font.style':   'normal',
          'font.variant': 'normal',
          'font.weight':  100,
          'font.stretch': 'normal',
          'font.size':    8.0,
          }

plt.rcParams.update(params)


# In[8]:


# Fit Gaussians to infer the accumulators threshold

fig, axs = plt.subplots(16, len(data_sets), figsize=(25, 50))
fig.subplots_adjust(hspace = .5, wspace=0.1)

threshold_acc_IF = {}

count_group = 0

# Let's create a function to model and create data
def func(x, a, x0, sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))

for data_set in data_sets_charact:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['UPR_ss'])
    group = group_data_sorted['sample']
        
    count_sample = 0
    
    for sample in group:
        
        ax = axs[count_sample, count_group]
        
        # colormap
        colorVal = 'b'
        
        x = POI[sample]['IF'][(POI[sample]['time_norm'] >= 20) & (POI[sample]['time_norm'] <= 25)]
        
        counts, bins, bars = ax.hist(x, bins = 100, color = colorVal)        
        
        # Executing curve_fit on noisy data
        try:
            popt, pcov = curve_fit(func, bins[1:], counts, maxfev=500000)
            
        except RuntimeError:
            print("Error - curve_fit failed")
            
        std_threshold = 3
            
        ym = func(bins[1:], popt[0], popt[1], popt[2])
        
        ax.plot(bins[1:], ym, c='r', label='Best fit')
        
        ax.plot([popt[1] + std_threshold*abs(popt[2]), popt[1] + std_threshold*abs(popt[2])], [0, np.max(counts)], '--', color = 'r')
                
        ax.set_title(str(data_summary['POI'][data_summary['sample'] == sample].values[0]))
        
        threshold_acc_IF[sample] = popt[1] + std_threshold*abs(popt[2])

        count_sample = count_sample + 1
        
    count_group = count_group + 1


# In[9]:


# Getting fraction of accululators

for data_set in data_sets_charact:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    count_sample = 0
    
    for sample in group:
        
        POI[sample]['n_cell'] = np.linspace(0, 1, len(POI[sample]))
        t_points = POI[sample].groupby(['time_norm'])
        
        lst = []
        
        for point in list(range(len(t_points))):

            t_point = t_points.get_group(list(t_points.groups)[point])
            
            lst.append(len(t_point['IF'][t_point['IF'] > threshold_acc_IF[sample]])/len(t_point['IF']))
                         
        POI_median[sample]['fraction_accumulators_IF'] = lst


# In[10]:


# Fit Gaussians to infer the accumulators threshold

fig, axs = plt.subplots(16, len(data_sets), figsize=(25, 50))
fig.subplots_adjust(hspace = .5, wspace=0.1)

threshold_acc_UPR = {}

count_group = 0

# Let's create a function to model and create data
def func(x, a, x0, sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))

for data_set in data_sets_charact:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['UPR_ss'])
    group = group_data_sorted['sample']
        
    count_sample = 0
    
    for sample in group:
        
        ax = axs[count_sample, count_group]
        
        # colormap
        colorVal = 'b'
        
        x = POI[sample]['UPR'][(POI[sample]['time_norm'] >= 20) & (POI[sample]['time_norm'] <= 25)]
        
        counts, bins, bars = ax.hist(x, bins = 100, color = colorVal)        
        
        # Executing curve_fit on noisy data
        try:
            popt, pcov = curve_fit(func, bins[1:], counts, maxfev=500000)
            
        except RuntimeError:
            print("Error - curve_fit failed")
            
        std_threshold = 3
            
        ym = func(bins[1:], popt[0], popt[1], popt[2])
        
        ax.plot(bins[1:], ym, c='r', label='Best fit')
        
        ax.plot([popt[1] + std_threshold*abs(popt[2]), popt[1] + std_threshold*abs(popt[2])], [0, np.max(counts)], '--', color = 'r')
                
        ax.set_title(str(data_summary['POI'][data_summary['sample'] == sample].values[0]))
        
        threshold_acc_UPR[sample] = popt[1] + std_threshold*abs(popt[2])

        count_sample = count_sample + 1
        
    count_group = count_group + 1


# In[11]:


# Getting fraction of accululators

for data_set in data_sets_charact:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    count_sample = 0
    
    for sample in group:
        
        POI[sample]['n_cell'] = np.linspace(0, 1, len(POI[sample]))
        t_points = POI[sample].groupby(['time_norm'])
        
        lst = []
        
        for point in list(range(len(t_points))):

            t_point = t_points.get_group(list(t_points.groups)[point])
            
            lst.append(len(t_point['UPR'][t_point['UPR'] > threshold_acc_UPR[sample]])/len(t_point['UPR']))
                         
        POI_median[sample]['fraction_accumulators_UPR'] = lst


# In[12]:


# Getting fraction of accululators with potential to recover

for data_set in data_sets_charact:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    count_sample = 0
    
    for sample in group:
        
        POI[sample]['n_cell'] = np.linspace(0, 1, len(POI[sample]))
        t_points = POI[sample].groupby(['time_norm'])
        
        lst = []
        
        for point in list(range(len(t_points))):

            t_point = t_points.get_group(list(t_points.groups)[point])
            
            lst.append(len(t_point['IF'][(t_point['IF'] > threshold_acc_IF[sample]) & (t_point['UPR'] > threshold_acc_UPR[sample])]) / len(t_point['UPR']))
                         
        POI_median[sample]['fraction_accumulators'] = lst


# In[13]:


# Fit Gaussians to infer the accumulators threshold

fig, axs = plt.subplots(16, len(data_sets), figsize=(25, 50))
fig.subplots_adjust(hspace = .5, wspace=0.1)

threshold_acc_UPR = {}

count_group = 0

# Let's create a function to model and create data
def func(x, a, x0, sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))

data_sets_selected = ['FB_1', 'FB_2', 'FB_3', 'FB_5']

for data_set in data_sets_selected:
    
    group = list(data_summary['sample'][(data_summary['POI'] == data_set) & (data_summary['DC'] == 1)])
        
    count_sample = 0
    
    for sample in group:
        
        ax = axs[count_sample, count_group]
        
        # colormap
        colorVal = 'b'
        
        x = POI[sample]['UPR'][(POI[sample]['time_norm'] >= 20) & (POI[sample]['time_norm'] <= 25)]
        
        counts, bins, bars = ax.hist(x, bins = 100, color = colorVal)        
        
        # Executing curve_fit on noisy data
        try:
            popt, pcov = curve_fit(func, bins[1:], counts, maxfev=500000)
            
        except RuntimeError:
            print("Error - curve_fit failed")
            
        std_threshold = 3
            
        ym = func(bins[1:], popt[0], popt[1], popt[2])
        
        ax.plot(bins[1:], ym, c='r', label='Best fit')
        
        ax.plot([popt[1] + std_threshold*abs(popt[2]), popt[1] + std_threshold*abs(popt[2])], [0, np.max(counts)], '--', color = 'r')
                
        ax.set_title(str(data_summary['POI'][data_summary['sample'] == sample].values[0]))
        
        threshold_acc_UPR[sample] = popt[1] + std_threshold*abs(popt[2])

        count_sample = count_sample + 1
        
    count_group = count_group + 1


# In[14]:


# Getting fraction of accululators

data_sets_selected = ['FB_1', 'FB_2', 'FB_3', 'FB_5']

for data_set in data_sets_selected:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    count_sample = 0
    
    for sample in group:
        
        IL = data_summary['UPR_ss'][data_summary['sample'] == sample].values[0]
        
        if np.isnan(IL) == False:
        
            sample_ref = data_summary['sample'][(data_summary['POI'] == data_set) & (data_summary['DC'] == 1)].values[0]

            POI[sample]['n_cell'] = np.linspace(0, 1, len(POI[sample]))
            t_points = POI[sample].groupby(['time_norm'])

            lst = []

            for point in list(range(len(t_points))):

                t_point = t_points.get_group(list(t_points.groups)[point])

                lst.append(len(t_point['UPR'][t_point['UPR'] > threshold_acc_UPR[sample_ref]])/len(t_point['UPR']))


            POI_median[sample]['fraction_accumulators_UPR'] = lst      
            
        else:
            
            POI_median[sample]['fraction_accumulators_UPR'] = [np.nan] * POI_median[sample]['UPR']


# In[15]:


# Fit Gaussians to infer the accumulators threshold

fig, axs = plt.subplots(16, len(data_sets), figsize=(25, 50))
fig.subplots_adjust(hspace = .5, wspace=0.1)

threshold_acc_IF = {}

count_group = 0

# Let's create a function to model and create data
def func(x, a, x0, sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))

data_sets_selected = ['FB_1', 'FB_2', 'FB_3', 'FB_5']

for data_set in data_sets_selected:
    
    group = list(data_summary['sample'][(data_summary['POI'] == data_set) & (data_summary['DC'] == 1)])
        
    count_sample = 0
    
    for sample in group:
        
        ax = axs[count_sample, count_group]
        
        # colormap
        colorVal = 'b'
        
        x = POI[sample]['IF'][(POI[sample]['time_norm'] >= 20) & (POI[sample]['time_norm'] <= 25)]
        
        counts, bins, bars = ax.hist(x, bins = 100, color = colorVal)        
        
        # Executing curve_fit on noisy data
        try:
            popt, pcov = curve_fit(func, bins[1:], counts, maxfev=500000)
            
        except RuntimeError:
            print("Error - curve_fit failed")
            
        std_threshold = 3
            
        ym = func(bins[1:], popt[0], popt[1], popt[2])
        
        ax.plot(bins[1:], ym, c='r', label='Best fit')
        
        ax.plot([popt[1] + std_threshold*abs(popt[2]), popt[1] + std_threshold*abs(popt[2])], [0, np.max(counts)], '--', color = 'r')
                
        ax.set_title(str(data_summary['POI'][data_summary['sample'] == sample].values[0]))
        
        threshold_acc_IF[sample] = popt[1] + std_threshold*abs(popt[2])

        count_sample = count_sample + 1
        
    count_group = count_group + 1


# In[16]:


# Getting fraction of accululators

data_sets_selected = ['FB_1', 'FB_2', 'FB_3', 'FB_5']

for data_set in data_sets_selected:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
    
    count_sample = 0
    
    for sample in group:
        
        IL = data_summary['IF_ss'][data_summary['sample'] == sample].values[0]
        
        if np.isnan(IL) == False:
        
            sample_ref = data_summary['sample'][(data_summary['POI'] == data_set) & (data_summary['DC'] == 1)].values[0]

            POI[sample]['n_cell'] = np.linspace(0, 1, len(POI[sample]))
            t_points = POI[sample].groupby(['time_norm'])

            lst = []

            for point in list(range(len(t_points))):

                t_point = t_points.get_group(list(t_points.groups)[point])

                lst.append(len(t_point['IF'][t_point['IF'] > threshold_acc_IF[sample_ref]])/len(t_point['IF']))


            POI_median[sample]['fraction_accumulators_IF'] = lst      
            
        else:
            
            POI_median[sample]['fraction_accumulators_IF'] = [np.nan] * POI_median[sample]['IF']


# In[17]:


def acc_func(UPR, th, slp, acc_basal):
        
    acc = []
        
    for x in UPR:
                
        if x <= th:
            
            y = acc_basal

        if x > th:
        
            y = acc_basal + slp * (x - th) 
            
        acc += [y]        
    
    return acc


# In[18]:


def acc_to_cap_func(acc, acc_basal):
    
    acc = np.array(acc)
        
    cap = acc - acc_basal
        
    return cap


# In[19]:


data_sets_selected = ['scFv']

popt = {}

count_group = 0

for data_set in data_sets_selected:
    
    group = data_summary[(data_summary['POI'] == data_set) & (data_summary['UPR_ss'] > 0)]
    group_data_sorted = group.sort_values(by=['UPR_ss'])
    group = group_data_sorted['sample']
    
    x = []
    y = []
    
    for sample in group:
                
        x += [data_summary['UPR_ss'][data_summary['sample'] == sample].values[0]]
                
        y += [np.max(POI_median[sample]['fraction_accumulators_UPR'][(POI_median[sample]['time_norm'] > 0) & (POI_median[sample]['time_norm'] < 24)])]      

    p = np.array([0.17, 1, 0.05])

    xdata = np.array(x)
    ydata = np.array(y)

    popt[count_group], pcov = curve_fit(acc_func, xdata, ydata, p)
    
    count_group = count_group + 1


# In[20]:


count_group = 0

acc = {}
cap = {}

for data_set in data_sets_selected:
    
    group = data_summary[(data_summary['POI'] == data_set) & (data_summary['UPR_ss'] > 0)]
    group_data_sorted = group.sort_values(by=['UPR_ss'])
    group = group_data_sorted['sample']
    
    x = []
    y = []
    
    for sample in group:
                
        x += [data_summary['UPR_ss'][data_summary['sample'] == sample].values[0]]
        
    xdata = np.linspace(0, 1, 1000)
    
    th, slp, acc_basal = popt[count_group]

    acc[count_group] = acc_func(xdata, th, slp, acc_basal)
    
    cap[count_group] = acc_to_cap_func(acc[count_group], acc_basal)
    
    count_group = count_group + 1


# In[21]:


# Correlation overshooted population is maximal vs induction
fig, axs = plt.subplots(1, 1)
fig.set_size_inches(8*0.9/4, 1.7, forward=True)
fig.subplots_adjust(hspace = 0, wspace=0)

count_group = 0

ind_th = []

# colormap
N = len(data_summary['UPR_ss'])
cmap = plt.get_cmap('Reds', N)

# Normalizer
norm = mpl.colors.Normalize(vmin=np.min(data_summary['UPR_ss']),                  vmax=np.max(data_summary['UPR_ss']))

# creating ScalarMappable
sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
sm.set_array([])

for data_set in data_sets_selected:
    
    group = data_summary[data_summary['POI'] == data_set]
    
    group_data_sorted = group.sort_values(by=['UPR_ss'])
    group = group_data_sorted['sample']
        
    ax = plt
    
    x_all = []
    y_all = []
        
    th, slp, acc_basal = popt[count_group]
    
    xdata = np.linspace(0, 1, 1000)

    capacity_th = 0.05
    
    ind_th += [(capacity_th/slp) + th]

    ax.plot(xdata, cap[count_group],                 '--', color = sm.to_rgba(0.5), linewidth = 1, alpha = 1)
    
    ax.plot([-1, 1], [capacity_th, capacity_th],                 '--', color = 'k', linewidth = 1, alpha = 0.3)
    
    ax.plot([ind_th[count_group], ind_th[count_group]], [-1, 1],                 '--', color = 'salmon', linewidth = 1, alpha = 0.5)
            
    ax.text(0, 0.25, 'UPR th.: ' + str(round(ind_th[count_group], 2)))

    x = []
    y = []
        
    for sample in group:
        colorVal = sm.to_rgba(data_summary['UPR_ss'][data_summary['sample'] == sample].values[0])
            
        x += [data_summary['UPR_ss'][data_summary['sample'] == sample].values[0]]
        
        y += [np.max(POI_median[sample]['fraction_accumulators_UPR'][(POI_median[sample]['time_norm'] > 0) & (POI_median[sample]['time_norm'] < 24)]) - acc_basal]
        
        ax.plot(x[-1], y[-1],                      'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = colorVal)
   
ax.xlim(-0.01, np.nanmax(x) * 1.1)
ax.ylim(-0.01, 0.3)    
ax.xlabel('UPR$_{ss}$ (RPU)')
ax.ylabel('Maximal fraction of UPR-defined accumulators')

plt.savefig('acc_opt.svg')


# In[22]:


experiment_date = data_summary['date'][data_summary['sample'] == 23].values[0]

folder = path + '\\data\\' + str(experiment_date) + '_REACTORS'

with open(path + '\\data\\cells_AF.CSV', 'r') as infile:
    data_AF = pd.read_csv(infile)
    data_AF['GRN-B/FSC'] = data_AF['GRN-B-HLin']/data_AF['FSC-HLin']
    data_AF['GRN-V/FSC'] = data_AF['GRN-V-HLin']/data_AF['FSC-HLin']
    data_AF['ORG-G/FSC'] = data_AF['ORG-G-HLin']/data_AF['FSC-HLin']
    data_AF['time_norm'] = (data_AF['time_s'] - np.nanmin(data_AF['time_s']))/60/60
    data_AF_gated = data_AF[(data_AF['FSC-HLin'] > 1*10**3) &                                    (data_AF['FSC-HLin'] < 2*10**3)].copy() 
    data_AF_median = data_AF_gated.groupby('time_s').median().reset_index()
    AF_vector = data_AF_median[-10:].median()

with open(path + '\\data\\cells_pTDH3-mScI.CSV', 'r') as infile:
    data_mScI = pd.read_csv(infile)
    data_mScI['ORG-G/FSC'] = data_mScI['ORG-G-HLin']/data_mScI['FSC-HLin'] 
    data_mScI['time_norm'] = (data_mScI['time_s'] - np.nanmin(data_mScI['time_s']))/60/60
    data_mScI_gated = data_mScI[(data_mScI['FSC-HLin'] > 1*10**3) &                                    (data_mScI['FSC-HLin'] < 2*10**3)].copy() 
    data_mScI_median = data_mScI_gated.groupby('time_s').median().reset_index()
    mScI_RPU = data_mScI_median['ORG-G/FSC'][-10:].median() - AF_vector['ORG-G/FSC']
    

with open(path + '\\data\\Guava_compensation.CSV', 'r') as infile:
        compensation_matrix = pd.read_csv(infile)

closest_data = []
        
for date in compensation_matrix['Date_f2']:
    closest_data.append(abs(experiment_date - date))
            
    a = closest_data.index(np.min(closest_data))
    correction_coef_FSC = 1/compensation_matrix.iloc[a][1]
    correction_coef_SSC = 1/compensation_matrix.iloc[a][2]
    correction_coef_GRN_V = 1/compensation_matrix.iloc[a][3]
    correction_coef_GRN_B = 1/compensation_matrix.iloc[a][4]
    correction_coef_ORG_G = 1/compensation_matrix.iloc[a][5]
    correction_coef_BLU_V = 1/compensation_matrix.iloc[a][6]


# In[23]:


# Sample 23 is the one I selected for example data

sample_selected = 23

# Make the target in ORG-G/FSC units
    
fluo_t0 = np.mean(POI[sample_selected][(POI[sample_selected]['time_norm'] < 0) & (POI[sample_selected]['time_norm'] >= -2)]                     .groupby(['time_h']).median()['ORG-G_FSC']) 

# This is to convert from cytometry units to RPU

target = (((0.15 / correction_coef_FSC) / 1.35) + fluo_t0) * mScI_RPU

print(target)


# In[24]:


fig, axs = plt.subplots(1, 1)
fig.set_size_inches(8*0.9/4, 1.7, forward=True)
fig.subplots_adjust(hspace = 0, wspace=0)

for data_set in data_sets:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
        
    ax = plt
    
    if data_set in data_sets_charact:
        
        color = 'salmon'
        
    else:
        color = 'cornflowerblue'

    for sample in group:
        
        x = data_summary['UPR_ss'][data_summary['sample'] == sample].values[0]

        y = data_summary['sec'][data_summary['sample'] == sample].values[0]
        
        if sample == 23:
            
            marker = '*'
            Markersize = 10
        
        else:
            
            marker = 'o'
            Markersize = 5
        
        if (data_summary['POI'][data_summary['sample'] == sample].values[0] == 'scFv'):
        
            plt.plot(x, y,                        marker, Markersize = Markersize, markeredgecolor = 'k', markeredgewidth = 0.2 , color = color)
        
        if (data_summary['DC'][data_summary['sample'] == sample].values[0] == 2):

            plt.plot(x, y,                        marker, Markersize = Markersize, markeredgecolor = 'k', markeredgewidth = 0.2 , color = color)
            
ax.plot([ind_th[0], ind_th[0]], [-1, 2],     '--', color = 'salmon', linewidth = 1, alpha = 0.5)

ax.plot([target, target], [-1, 2],     '--', color = 'k', linewidth = 1, alpha = 0.5)
        
plt.xlabel('UPR average levels 16-24 h (RPU) ')
plt.ylabel('Norm. sec. lev. $_{24h}$')
ax.xlim(-0.02, np.nanmax(data_summary['UPR_ss']) * 1.1)
plt.ylim(-0.1, 2)  
    
plt.savefig('SL' + '_FB' + '.svg')


# In[25]:


fig, axs = plt.subplots(1, 1)
fig.set_size_inches(8*0.9/4, 1.7, forward=True)
fig.subplots_adjust(hspace = 0, wspace=0)

for data_set in data_sets:
    
    group = data_summary['sample'][data_summary['POI'] == data_set]
        
    ax = plt
    
    if data_set in data_sets_charact:
        color = 'Reds'
        
    else:
        color = 'Blues'
    
    # colormap
    N = len(data_summary['UPR_ss'])
    cmap = plt.get_cmap(color, N)

    # Normalizer
    norm = mpl.colors.Normalize(vmin=np.min(data_summary['UPR_ss']),                      vmax=np.max(data_summary['UPR_ss']))

    # creating ScalarMappable
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])

    for sample in group:
        
        colorVal = sm.to_rgba(data_summary['UPR_ss'][data_summary['sample'] == sample].values[0])
        
        x = [np.max(POI_median[sample]['fraction_accumulators_UPR'][(POI_median[sample]['time_norm'] > 0) & (POI_median[sample]['time_norm'] < 24)])]

        y = data_summary['sec'][data_summary['sample'] == sample].values[0]
        
        if (data_summary['POI'][data_summary['sample'] == sample].values[0] == 'scFv'):
        
            plt.plot(x, y,                        'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = colorVal)
        
        if (data_summary['DC'][data_summary['sample'] == sample].values[0] == 2):

            plt.plot(x, y,                        'o', MarkerSize = 5, markeredgecolor = 'k', markeredgewidth = 0.2 , color = colorVal)
        
ax.xlabel('Maximal fraction of UPR-defined accumulators')
ax.ylabel('Norm. sec. lev. $_{24h}$')
ax.xlim(-0.02, 0.3)
ax.ylim(-0.1, 2)  
    
plt.savefig('SL' + '_acc_FB' + '.svg')


# In[26]:


#plot distributions

FB_experiments = data_summary[data_summary['DC'] == 2]

experiment_selected = FB_experiments['POI'][FB_experiments['sec'] == np.nanmax(FB_experiments['sec'])].values[0]

FB_sample_selected = FB_experiments['sample'][FB_experiments['sec'] == np.nanmax(FB_experiments['sec'])].values[0]

FL_sample_selected = data_summary['sample'][(data_summary['POI'] == experiment_selected) & (data_summary['DC'] == 1)].values[0]

example_samples_selected = [FL_sample_selected, FB_sample_selected]

fig, axs = plt.subplots(1, 1)
fig.set_size_inches(8*0.4, 1.7, forward=True)
fig.subplots_adjust(hspace = 0, wspace=0)
    
color = ['palegreen' , 'cornflowerblue']

color_mean = ['g' , 'b'] 

variable = 'IF'

for sample in example_samples_selected:

    ax = plt

    POI[sample]['n_cell'] = np.linspace(0,1,len(POI[sample]))
    t_points = POI[sample].groupby(['time_norm'])

    mean = []

    for point in list(range(len(t_points))):

        t_point = t_points.get_group(list(t_points.groups)[point])

        x = t_point['time_norm'] * np.ones(len(t_point[variable])) + np.random.normal(scale=0.1, size=len(t_point[variable]))

        y = t_point[variable]
        
        mean += [np.mean(t_point[variable])]

        ax.scatter(x, y,                     s=1, facecolors='none', color = color[example_samples_selected.index(sample)], alpha=0.05, rasterized=True)
        
        
    if (data_summary['DC'][data_summary['sample'] == sample].values[0] == 2):
        
        marker = 'o'
        
    else:
        
        marker ='--'
              
    ax.plot(POI_median[sample]['time_norm'], mean,             marker, color = color_mean[example_samples_selected.index(sample)], linewidth = 1, markersize = 1)   


    ax.ylabel('iPOI (RPU)')
    ax.xlabel('Time (h)')
    ax.ylim(0, 0.5)
    ax.xlim(-3, 24)
    
plt.savefig('FB_pop_iPOI.svg')


# In[27]:


#plot distributions

fig, axs = plt.subplots(1, 1)
fig.set_size_inches(8*0.4, 1.7, forward=True)
fig.subplots_adjust(hspace = 0, wspace=0)
    
color = ['salmon' , 'cornflowerblue'] 

color_mean = ['r' , 'b'] 

variable = 'UPR'

for sample in example_samples_selected:

    ax = plt

    POI[sample]['n_cell'] = np.linspace(0,1,len(POI[sample]))
    t_points = POI[sample].groupby(['time_norm'])
    
    
    mean = []

    for point in list(range(len(t_points))):

        t_point = t_points.get_group(list(t_points.groups)[point])

        x = t_point['time_norm'] * np.ones(len(t_point[variable])) + np.random.normal(scale=0.1, size=len(t_point[variable]))

        y = t_point[variable]
        
        mean += [np.mean(t_point[variable])]

        ax.scatter(x, y,                     s=1, facecolors='none', color = color[example_samples_selected.index(sample)], alpha=0.05, rasterized=True)
        
    if (data_summary['DC'][data_summary['sample'] == sample].values[0] == 2):
        
        marker = 'o'
        
    else:
        
        marker ='--'
              
    ax.plot(POI_median[sample]['time_norm'], mean,             marker, color = color_mean[example_samples_selected.index(sample)], linewidth = 1, markersize = 1)        

    ax.ylabel('UPR (RPU)')
    ax.xlabel('Time (h)')
    ax.ylim(0, 1)
    ax.xlim(-3, 24)

ax.plot([-3, 24], [target, target],         '--', color = 'k', linewidth = 1)
    
plt.savefig('FB_pop_UPR.svg')


# In[28]:


#plot distributions

fig, axs = plt.subplots(1, 1)
fig.set_size_inches(8*0.4, 1.7, forward=True)
fig.subplots_adjust(hspace = 0, wspace=0)
    
color = ['salmon' , 'cornflowerblue'] 

color_mean = ['r' , 'b'] 

variable = 'UPR'

for sample in example_samples_selected:

    ax = plt
    
    # Make the target in ORG-G/FSC units
    
    fluo_t0 = np.mean(POI[sample][(POI[sample]['time_norm'] < 0) & (POI[sample]['time_norm'] >= -2)]                         .groupby(['time_h']).median()['ORG-G_FSC'])       

    target = (0.15 * (2 / 1.35)) + fluo_t0
    
    if data_summary['DC'][data_summary['sample'] == sample].values[0] == 2:
        # Plot the LEDs activation only for the feedback one
        # here put the last time point that is not in the output ftom the platform
        time_light = list(LED[sample]['time_norm']) + [POI_median[sample]['time_norm'].iloc[-1]]
        intensity_light = list(LED[sample]['intensity'])

        for i in range(0, len(time_light)-1):
            x = np.linspace(time_light[i], time_light[i+1], 2)
            y = intensity_light[i] /20
            ax.fill_between(x, y, color = 'skyblue', edgecolor = None)

    ax.xlabel('Time (h)')
    ax.ylim(0, 10**1)
    ax.xlim(-3, 24)
    
plt.savefig('light_stimulation.svg')


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




