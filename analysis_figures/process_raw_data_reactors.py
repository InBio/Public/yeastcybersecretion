#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Python modules
import sys

# Main path: to be changed with the path in which you saved the repository
path = 'C:\\Users\\lifeware\\Desktop\\20221104_paper' 

# In-house modules
sys.path.append(path + '\\code')

import process_data


# In[2]:


# Write the experiment date in the order that you want them

mNeon_non_sec = [20211027]

mNeon = [20220412]

XylC = [20211130]

Xyl2 = [20211103]

hPON1 = [20220215]

scFv = [20210817, 20211115]

amy = [20210603, 20210830]

characterization_experiments = mNeon_non_sec + mNeon + XylC + Xyl2 + hPON1 + scFv + amy 

KO_HAC1 = [20220504, 20220518]

scFv_N = [20220608]

Xyl2_TM = [20220629]

FB_1 = [20211117]

FB_2 = [20211124]

FB_3 = [20220125]

FB_5 = [20220510]

experiment_dates = characterization_experiments + KO_HAC1 + scFv_N +  Xyl2_TM + FB_1 + FB_2 + FB_3 + FB_5

for experiment_date in experiment_dates:
    print(experiment_date)
    process_data.process_data_dynamics(experiment_date, path)


# In[ ]:





# In[ ]:




