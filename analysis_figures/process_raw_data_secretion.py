#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Python modules
import sys

# Main path
path = 'C:\\Users\\lifeware\\Desktop\\20221104_paper'

# In-house modules
sys.path.append(path + '\\code')

import secretion_data_processing


# In[2]:


## Write the experiments date in yyyymmdd format

# The reference batches are used to normalize the secretion levels with a 24h batch of mNeonGreen secreted

reference_batch = [20210603, 20210604, 20210605]

# Write the experiment (secretion measurement) date in the order that you want them (same as above)
   
mNeon_non_sec = [20211029] 

mNeon = [20220414]

XylC = [20211202]

Xyl2 = [20211105]

hPON1 = [20220217]

scFv = [20210819, 20211117]

amy = [20210605, 20210831]

characterization_experiments = mNeon_non_sec + mNeon + XylC + Xyl2 + hPON1 + scFv + amy

KO_HAC1 = [20220506]

scFv_N = [20220610]

Xyl2_TM = [20220701]

FB_1 = [20211119]

FB_2 = [20211126]

FB_3 = [20220127]

FB_5 = [20220512]

bead_dates = characterization_experiments + KO_HAC1 + scFv_N + Xyl2_TM + FB_1 + FB_2 + FB_3 + FB_5

# Write the experiment (reactors setting up) date in the same order as above 

mNeon_non_sec = [20211027]

mNeon = [20220412]

XylC = [20211130]

Xyl2 = [20211103]

hPON1 = [20220215]

scFv = [20210817, 20211115]

amy = [20210603, 20210830]

characterization_experiments = mNeon_non_sec + mNeon + XylC + Xyl2 + hPON1 + scFv + amy 

KO_HAC1 = [20220504]

scFv_N = [20220608]

Xyl2_TM = [20220629]

FB_1 = [20211117]

FB_2 = [20211124]

FB_3 = [20220125]

FB_5 = [20220510]

experiment_dates = characterization_experiments + KO_HAC1 + scFv_N +  Xyl2_TM + FB_1 + FB_2 + FB_3 + FB_5

folder_source =  path + '\\data'

folder_destiny = folder_source

secretion_data_processing.secretion_reference_batch(reference_batch, folder_source, folder_destiny)

secretion_data_processing.secretion_processing(experiment_dates, bead_dates, reference_batch, folder_source, folder_destiny)


# In[ ]:





# In[ ]:




