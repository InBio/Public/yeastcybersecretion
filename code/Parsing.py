import numpy as np
import pandas as pd

def sec_parse(experiment_dates, sec_experiment_dates, metadata, POI_median, path, events_th): 
# Let's load the secretion data as dataframes, but only the ratiometric data

    folder =  path + '\\data\\'

    sec_raw = {}
    sec = {}

    for sec_experiment_date in sec_experiment_dates:

        experiment_date = experiment_dates[sec_experiment_dates.index(sec_experiment_date)]

        with open(folder + 'data_sec_' + str(sec_experiment_date) + '.csv', 'r') as infile:
            df = pd.read_csv(infile)

        df['reactor'] = ''

        reactors = list(metadata['reactor'][metadata['date'] == experiment_date])

        for reactor in reactors:

            df['reactor'].loc[reactors.index(reactor)] = reactor

        # Now I go with the selected reactors, that is without the outliers

        for reactor in reactors:

            sample = metadata['sample'][(metadata['date'] == experiment_date) &  \
                                       (metadata['reactor'] == reactor)].values[0]

            if df['events'][df['reactor'] == reactor].values[0] < events_th:

                sec_raw[sample] = np.nan

            if df['events'][df['reactor'] == reactor].values[0] >= events_th:

                sec_raw[sample] = df['secretion'][df['reactor'] == reactor].values[0]

            # Normalize to account for the sensor population (Not always needed)
            if metadata['sensor'][(metadata['reactor'] == reactor) & (metadata['date'] == experiment_date)].values[0] == 1:
                F_end = np.nanmean(POI_median[sample]['fraction'][(POI_median[sample]['time_norm'] >= 20) & \
                                                          (POI_median[sample]['time_norm'] <= 24)])
            else:
                F_end = 1

            sec[sample] = sec_raw[sample]/F_end

    return sec_raw, sec
    
    
def derived_data(sensor_median, POI_median, sec, metadata, end, path):
    # Compute induction levels and generate a vector with them to combine with time
    
    folder =  path + '\\data\\'

    samples = metadata['sample']

    sensor_ss = {}

    POI_sec = pd.DataFrame()

    POI_sec['sample'] = metadata['sample']
    
    # Load the induction data saved    
    
    with open(folder + '\\induction_levels_registry.csv', 'r') as infile:
        all_iniduction = pd.read_csv(infile)
    
    # Make the induction levels relative to the max among all samples
    
    a = np.max(all_iniduction['induction_raw'])

    t_sec = []
    sec_values = []
    
    induction_levels = {}

    for sample in samples:
        
        induction = sensor_median[sample]['induction'][(sensor_median[sample]['time_norm'] > 20) & \
                                                          (sensor_median[sample]['time_norm'] < 25)]
        
        sensor_ss[sample] =  np.mean([np.mean(induction)])
        
        induction_levels[sample] = sensor_ss[sample]/a
        
        print('POI: ' + str(metadata['POI'][metadata['sample'] == sample].values[0]) + ', sample: ' + \
              str(metadata['sample'][metadata['sample'] == sample].values[0]))

        induction = []

        induction_rt = []

        # Get rid of negative values of induction when the DC = 0
        if metadata['DC'][metadata['sample'] == sample].values[0] == 0:
            induction_levels[sample] = 0

            before = list(np.zeros(len(sensor_median[sample][sensor_median[sample]['time_norm']<0])))

            after = list(np.zeros(len(sensor_median[sample][sensor_median[sample]['time_norm']>=0])))

            after_rt = list(np.zeros(len(sensor_median[sample][sensor_median[sample]['time_norm']>=0])))

        else:

            before = list(np.zeros(len(sensor_median[sample][sensor_median[sample]['time_norm']<0])))

            after = [induction_levels[sample]] * len(sensor_median[sample][sensor_median[sample]['time_norm']>=0])

            after_rt = list(sensor_median[sample]['induction'][sensor_median[sample]['time_norm']>=0].values/a)

        induction = before + after

        induction_rt = before + after_rt

        POI_median[sample]['induction'] = induction

        POI_median[sample]['induction_rt'] = induction_rt

        sec_values += [sec[sample]] 
        
        # I select as secretion sampling time the one closer to 24h, since I sample just after the cytometry measurement

        closest = list(abs(POI_median[sample]['time_norm'] - 24))
        
        idx = closest.index(np.min(closest))

        t_sec += [POI_median[sample]['time_norm'].iloc[idx]]

    POI_sec['time_norm'] = t_sec
    POI_sec['sec_raw'] = sec_values

    return POI_median, POI_sec, induction_levels

def slopes(x, t, smooth, t_start, t_end):
# Compute slopes from the dynamics data (smoothing can be apalyed)

    from scipy.signal import savgol_filter

    t_temp = t.loc[(t >= t_start) & (t <= t_end)]
    slope = []
    time = []
    if smooth == 1:
        y = savgol_filter(x, len(t_temp), 10) # window size, polynomial order 

    if smooth == 0:
        y = x

    for i in range(0, len(t_temp.index)-1):

        a= (y[t_temp.index[i + 1]] - y[t_temp.index[i]])/(t[t_temp.index[i + 1]]\
                                                          - t[t_temp.index[i]])
        b = t[t_temp.index[i]]

        slope.append(a)
        time.append(b)

    return slope, time


def steady_states(x, variable, t_start, t_end, metadata):
# Compute steady state values of x[variable] between t_start and t_end

    samples = metadata['sample']

    x_ss = []

    for sample in samples:
        t = x[sample]['time_norm']
        x_ss.append(np.mean(x[sample][variable][(t >= t_start) & (t <= t_end)]))

    return x_ss