def get_processed_data(experiment_dates, path, outliers):
    import pandas as pd
    import numpy as np        

    def check_ouliers(outliers, metadata, sample):

        for outlier in outliers:

            if (outlier[0] == metadata['POI'][metadata['sample'] == sample].values[0]) & \
            (outlier[1] == metadata['replica'][metadata['sample'] == sample].values[0]) & \
            (outlier[2] == metadata['reactor'][metadata['sample'] == sample].values[0]):

                answer = True
                return answer
                break

            else:
                answer = False
                pass

        return answer

    metadata_lst = []

    for experiment_date in experiment_dates:

        folder = path + '\\data\\' + str(experiment_date) + '_REACTORS'

        with open(folder + '\\' + str(experiment_date) + '_metadata.csv') as infile:
            data = pd.read_csv(infile)

        metadata_lst.append(data)

    metadata = pd.concat(metadata_lst).reset_index()
    metadata['sample'] = list(range(1, len(metadata)+1))

    cells_raw = {}
    cells_corrected = {}
    cells = {}
    sensor = {}
    POI = {}
    sensor_median = {}
    POI_median = {}
    LED = {}

    for sample in metadata['sample']:

        print('POI: ' + str(metadata['POI'][metadata['sample'] == sample].values[0]) + ', sample: ' + str(metadata['sample'][metadata['sample'] == sample].values[0]))

        folder = path + '\\data\\' + str(metadata['date'][metadata['sample'] == sample].values[0]) + \
        '\\' + str(metadata['date'][metadata['sample'] == sample].values[0]) + '_REACTORS' + \
        '\\data_processed\\reactor-' + str(metadata['reactor'][metadata['sample'] == sample].values[0])

        answer = check_ouliers(outliers, metadata, sample)
        
        if metadata['sensor'][metadata['sample'] == sample].values[0] == 1:
            use_sensor = True
            
        if metadata['sensor'][metadata['sample'] == sample].values[0] == 0:
            use_sensor = False

        if answer == True:
            print('Sample above is OUT: counted as outlier')
            metadata = metadata.drop(metadata[metadata['sample'] == sample].index[0])

        if answer == False:
            print('Sample above is LOADED')
            # Load processed data:
            with open(folder + '\\LED_.csv', 'r') as infile:
                LED[sample] = pd.read_csv(infile)
            with open(folder + '\\cells_raw_.csv', 'r') as infile:
                cells_raw[sample] = pd.read_csv(infile)
            with open(folder + '\\cells_corrected_.csv', 'r') as infile:
                cells_corrected[sample] = pd.read_csv(infile)
            with open(folder + '\\cells_.csv', 'r') as infile:
                cells[sample] = pd.read_csv(infile)    
            with open(folder + '\\POI_.csv', 'r') as infile:
                POI[sample] = pd.read_csv(infile)
            with open(folder + '\\POI_median_.csv', 'r') as infile:
                POI_median[sample] = pd.read_csv(infile)

            if use_sensor == True:
                with open(folder + '\\sensor_.csv', 'r') as infile:
                    sensor[sample] = pd.read_csv(infile)
                with open(folder + '\\sensor_median_.csv', 'r') as infile:
                    sensor_median[sample] = pd.read_csv(infile)

            if use_sensor == False:
                    sensor[sample] = pd.DataFrame()
                    sensor_median[sample] = pd.DataFrame()
                      
    return LED, cells_raw, cells_corrected, cells, POI, POI_median, metadata, sensor, sensor_median