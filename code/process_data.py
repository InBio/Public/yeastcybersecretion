def process_data_dynamics(experiment_date, path_all):
    
    import pandas as pd
    import numpy as np
    import os    
    from scipy.signal import savgol_filter
    import guava2data

    folder = path_all + '\\data\\' + str(experiment_date) + '\\' + str(experiment_date) + '_REACTORS'
    
    with open(path_all + '\\data\\Guava_compensation.CSV', 'r') as infile:
        compensation_matrix = pd.read_csv(infile)
        
    # Here take the data for AF and RPU units    
    with open(path_all + '\\data\\cells_AF.CSV', 'r') as infile:
        data_AF = pd.read_csv(infile)
        data_AF['GRN-B/FSC'] = data_AF['GRN-B-HLin']/data_AF['FSC-HLin']
        data_AF['GRN-V/FSC'] = data_AF['GRN-V-HLin']/data_AF['FSC-HLin']
        data_AF['ORG-G/FSC'] = data_AF['ORG-G-HLin']/data_AF['FSC-HLin']
        data_AF['time_norm'] = (data_AF['time_s'] - np.nanmin(data_AF['time_s']))/60/60
        data_AF_gated = data_AF[(data_AF['FSC-HLin'] > 1*10**3) & \
                                       (data_AF['FSC-HLin'] < 2*10**3)].copy() 
        data_AF_median = data_AF_gated.groupby('time_s').median().reset_index()
        AF_vector = data_AF_median[-10:].median()
        
    with open(path_all + '\\data\\cells_pTDH3-mNG.CSV', 'r') as infile:
        data_mNG = pd.read_csv(infile)
        data_mNG['GRN-B/FSC'] = data_mNG['GRN-B-HLin']/data_mNG['FSC-HLin']
        data_mNG['time_norm'] = (data_mNG['time_s'] - np.nanmin(data_mNG['time_s']))/60/60
        data_mNG_gated = data_mNG[(data_mNG['FSC-HLin'] > 1*10**3) & \
                                       (data_mNG['FSC-HLin'] < 2*10**3)].copy() 
        data_mNG_median = data_mNG_gated.groupby('time_s').median().reset_index()
        mNG_RPU = data_mNG_median['GRN-B/FSC'][-10:].median() - AF_vector['GRN-B/FSC']
        
    with open(path_all + '\\data\\cells_pTDH3-mCer.CSV', 'r') as infile:
        data_mCer = pd.read_csv(infile)
        data_mCer['GRN-V/FSC'] = data_mCer['GRN-V-HLin']/data_mCer['FSC-HLin']
        data_mCer['time_norm'] = (data_mCer['time_s'] - np.nanmin(data_mCer['time_s']))/60/60
        data_mCer_gated = data_mCer[(data_mCer['FSC-HLin'] > 1*10**3) & \
                                       (data_mCer['FSC-HLin'] < 2*10**3)].copy() 
        data_mCer_median = data_mCer_gated.groupby('time_s').median().reset_index()
        mCer_RPU = data_mCer_median['GRN-V/FSC'][-10:].median() - AF_vector['GRN-V/FSC']
        
    with open(path_all + '\\data\\cells_pTDH3-mScI.CSV', 'r') as infile:
        data_mScI = pd.read_csv(infile)
        data_mScI['ORG-G/FSC'] = data_mScI['ORG-G-HLin']/data_mScI['FSC-HLin'] 
        data_mScI['time_norm'] = (data_mScI['time_s'] - np.nanmin(data_mScI['time_s']))/60/60
        data_mScI_gated = data_mScI[(data_mScI['FSC-HLin'] > 1*10**3) & \
                                       (data_mScI['FSC-HLin'] < 2*10**3)].copy() 
        data_mScI_median = data_mScI_gated.groupby('time_s').median().reset_index()
        mScI_RPU = data_mScI_median['ORG-G/FSC'][-10:].median() - AF_vector['ORG-G/FSC']

    metadata =  pd.read_csv(folder + '\\' + str(experiment_date) + '_metadata.csv')
    reactors = metadata['reactor']
    cells_raw = {}
    cells_corrected = {}
    cells = {}
    sensor = {}
    POI = {}
    sensor_median = {}
    sensor_mean = {}
    POI_median = {}
    POI_mean = {}
    LED = {}
    induction_starts = {}
    
    for reactor in reactors:
        print('Reactor: ' + str(metadata['reactor'][metadata['reactor'] == reactor].values[0]))
        
        path = folder + '\\reactor-data\\reactor-' + str(reactor)

        # Load cytometry data:
        with open(path + '\\cells.csv', 'r') as infile:
            cells_raw[reactor] = pd.read_csv(infile)

        # Correction for cytometry metrics after Guava technician visits
        closest_data = []
        closest_data_AF = []

        for date in compensation_matrix['Date_f2']:
            closest_data.append(abs(experiment_date - date))
            
        a = closest_data.index(np.min(closest_data))
        correction_coef_FSC = 1/compensation_matrix.iloc[a][1]
        correction_coef_SSC = 1/compensation_matrix.iloc[a][2]
        correction_coef_GRN_V = 1/compensation_matrix.iloc[a][3]
        correction_coef_GRN_B = 1/compensation_matrix.iloc[a][4]
        correction_coef_ORG_G = 1/compensation_matrix.iloc[a][5]
        correction_coef_BLU_V = 1/compensation_matrix.iloc[a][6]
        
        cells_corrected[reactor] = cells_raw[reactor].copy()
        cells_corrected[reactor]['FSC-HLin'] = cells_raw[reactor]['FSC-HLin'] * correction_coef_FSC
        cells_corrected[reactor]['SSC-HLin'] = cells_raw[reactor]['SSC-HLin'] * correction_coef_SSC
        cells_corrected[reactor]['GRN-V-HLin'] = cells_raw[reactor]['GRN-V-HLin'] * correction_coef_GRN_V
        cells_corrected[reactor]['GRN-B-HLin'] = cells_raw[reactor]['GRN-B-HLin'] * correction_coef_GRN_B
        cells_corrected[reactor]['ORG-G-HLin'] = cells_raw[reactor]['ORG-G-HLin'] * correction_coef_ORG_G
        cells_corrected[reactor]['BLU-V-HLin'] = cells_raw[reactor]['BLU-V-HLin'] * correction_coef_ORG_G
        
        # Load LED data: 
        with open(path + '\\LEDs.csv', 'r') as infile:
            LED[reactor] = pd.read_csv(infile)

        ######## CHECK THIS OUT IF PROBLEMS WITH INDUCTION TIME POINT ########
        # When the DC is 0 I can not find the induction point, so I take the next reactor (or the previos if reactor 8)    
        if metadata['DC'][metadata['reactor'] == reactor].values[0] == 0:
            if reactor is not 8:
                path_LED = folder + '\\reactor-data\\reactor-' + str(reactor + 1)

            else:
                path_LED = folder + '\\reactor-data\\reactor-' + str(reactor - 1)

            with open(path_LED + '\\LEDs.csv', 'r') as infile:
                LED_TEMP = pd.read_csv(infile)
                
            LED[reactor]['time_s'] = LED_TEMP['time_s']
            LED[reactor]['intensity'] = np.zeros(len(LED[reactor]['time_s']))   
            # Computing time in hours units
            LED[reactor]['time_h'] = (LED[reactor]['time_s'] - \
            cells_raw[reactor]['time_s'].iloc[0])/60/60
            # Computing when induction starts
            induction_starts_idx = min(LED_TEMP[LED_TEMP['intensity'] > 0].index.values)
            induction_starts[reactor] = LED[reactor]['time_h'].iloc[induction_starts_idx] 

        if metadata['DC'][metadata['reactor'] == reactor].values[0] > 0:
            # Computing time in hours units
            LED[reactor]['time_h'] = (LED[reactor]['time_s'] - \
            cells_raw[reactor]['time_s'].iloc[0])/60/60
            # Computing when induction starts
            induction_starts_idx = min(LED[reactor][LED[reactor]['intensity'] > 0].index.values)
            induction_starts[reactor] = LED[reactor]['time_h'].iloc[induction_starts_idx] 

        # Normalize time to induction
        LED[reactor]['time_norm'] = LED[reactor]['time_h'] - induction_starts[reactor]
        
        # Gatting based in FSC and SSC
        cells[reactor] = cells_corrected[reactor][(cells_corrected[reactor]['FSC-HLin'] > 1*10**3) & \
                                   (cells_corrected[reactor]['FSC-HLin'] < 2*10**3)].copy() 

        # Normalize fluorescence by FSC
        cells[reactor]['GRN-V_FSC'] = cells[reactor]['GRN-V-HLin']/cells[reactor]['FSC-HLin']
        cells[reactor]['BLU-V_FSC'] = cells[reactor]['BLU-V-HLin']/cells[reactor]['FSC-HLin']
        cells[reactor]['GRN-B_FSC'] = cells[reactor]['GRN-B-HLin']/cells[reactor]['FSC-HLin']
        cells[reactor]['ORG-G_FSC'] = cells[reactor]['ORG-G-HLin']/cells[reactor]['FSC-HLin']

        # Computing time in hours units
        cells[reactor]['time_h'] = (cells[reactor]['time_s'] - \
        cells[reactor]['time_s'].iloc[0])/60/60 

        # Normalize time to induction
        cells[reactor]['time_norm'] = cells[reactor]['time_h'] - induction_starts[reactor]
        
        if metadata['sensor'][metadata['reactor'] == reactor].values[0] == 1:
            # Separate the sensor strain from the POI strain
            sensor[reactor] = cells[reactor][cells[reactor]['BLU-V_FSC'] > 6*10**-2].reset_index()
            POI[reactor] = cells[reactor][cells[reactor]['BLU-V_FSC'] < 3*10**-2].reset_index()
                    
            # Normalize to RPU
            
            fluo_t0 = np.mean(POI[reactor][(POI[reactor]['time_norm'] < 0) & (POI[reactor]['time_norm'] >= -2)]\
                         .groupby(['time_h']).median()['GRN-B_FSC'])
                        
            POI[reactor]['IF'] = (POI[reactor]['GRN-B_FSC'].copy() - fluo_t0)/mNG_RPU
            
            fluo_t0 = np.mean(POI[reactor][(POI[reactor]['time_norm'] < 0) & (POI[reactor]['time_norm'] >= -2)]\
                         .groupby(['time_h']).median()['ORG-G_FSC'])            
                        
            POI[reactor]['UPR'] = (POI[reactor]['ORG-G_FSC'].copy() - fluo_t0)/mScI_RPU
                        
            fluo_t0 = np.mean(sensor[reactor][(sensor[reactor]['time_norm'] < 0) & (sensor[reactor]['time_norm'] >= -2)]\
                         .groupby(['time_h']).median()['ORG-G_FSC'])
                
            sensor[reactor]['induction'] = (sensor[reactor]['ORG-G_FSC'].copy() - fluo_t0)/mScI_RPU
            
            # Compute medians
            sensor_median[reactor] = sensor[reactor].groupby(['time_h']).median().reset_index()
            POI_median[reactor] = POI[reactor].groupby(['time_h']).median().reset_index()

            # Compute growth rates
            # Assume growth rate of sensor strain is 0.4h^-1
            s = []
            p = []

            points_slope_gr = 5 # corresponds to 4 hours, 2 cell generations

            for i in range(0, len(sensor_median[reactor]['time_h'])):
                s.append(len(sensor[reactor][sensor[reactor]['time_h'] == sensor_median[reactor]['time_h'].iloc[i]]))
                
            for i in range(0, len(POI_median[reactor]['time_h'])):
                p.append(len(POI[reactor][POI[reactor]['time_h'] == POI_median[reactor]['time_h'].iloc[i]]))

            sensor_median[reactor]['events'] = s
            POI_median[reactor]['events'] = p

            POI_median[reactor]['ratio'] = POI_median[reactor]['events']\
            /sensor_median[reactor]['events']

            POI_median[reactor]['log_ratio'] = np.log(POI_median[reactor]['ratio'])
            
            # here compute the fraction
            R = POI_median[reactor]['ratio']
            F = R/(R + 1)
            POI_median[reactor]['fraction'] = F

            a = []
            
            for i in range(1, len(POI_median[reactor]['time_h'])-points_slope_gr):
                slope = np.polyfit(POI_median[reactor]['time_norm'][i:i+points_slope_gr], \
                                   POI_median[reactor]['log_ratio'][i:i+points_slope_gr],1)

                a.append(slope[0]+0.4)

            a = a + [np.mean(a[-10:-1])]*points_slope_gr
            a = [a[0]] + a

            # Filter to reduce noise
            gr_filtered = savgol_filter(a, points_slope_gr, 1)

            POI_median[reactor]['gr'] = gr_filtered
            
            #Save data in csv
            path_out = folder + '\\data_processed\\reactor-' + str(reactor) + '\\' 
            os.makedirs(path_out, exist_ok=True)

            sensor[reactor].to_csv(path_out + 'sensor_' + '.csv', index = False)
            sensor_median[reactor].to_csv(path_out + 'sensor_median_' + '.csv', index = False)
            
            # Here I save the induction levels to compute a factor accross experiments
            
            path_overall = path_all + '\\data\\induction_levels_registry.csv'
            
            df = pd.DataFrame()
            
            df['date'] = [experiment_date]
            df['induction_raw'] = [np.mean(sensor_median[reactor]['induction'][(sensor_median[reactor]['time_norm'] > 20) & \
                                                          (sensor_median[reactor]['time_norm'] < 25)])]
                        
            df.to_csv(path_overall, mode='a', index = False , header=not os.path.exists(path_overall))
            
        if metadata['sensor'][metadata['reactor'] == reactor].values[0] == 0:

            POI[reactor] = cells[reactor]

            # I do remove autofluorescence, because lekeage could happen
            
            fluo_t0 = np.mean(POI[reactor][(POI[reactor]['time_norm'] < 0) & (POI[reactor]['time_norm'] >= -2)]\
                         .groupby(['time_h']).median()['GRN-B_FSC'])
                        
            POI[reactor]['IF'] = (POI[reactor]['GRN-B_FSC'].copy() - fluo_t0)/mNG_RPU
            
            fluo_t0 = np.mean(POI[reactor][(POI[reactor]['time_norm'] < 0) & (POI[reactor]['time_norm'] >= -2)]\
                         .groupby(['time_h']).median()['ORG-G_FSC'])            
                        
            POI[reactor]['UPR'] = (POI[reactor]['ORG-G_FSC'].copy() - fluo_t0)/mScI_RPU
            
            # Compute medians
            POI_median[reactor] = POI[reactor].groupby(['time_h']).median().reset_index()

            # Compute growth rates
            with open(folder + '\\reactor-data\\reactor-' + str(reactor) + '\\dilutions.csv', 'r') as infile:
                dl = pd.read_csv(infile)

            dl['time_h'] = ((dl['time_s'] - \
                        cells_raw[reactor]['time_s'].iloc[0])/60/60) - induction_starts[reactor]

            # Widow of 2 hours to assess growht rate
            t_window = 2  
            t_start = dl['time_h'].iloc[0]
            t_end = t_start

            gr = pd.DataFrame()

            while t_end < dl['time_h'].iloc[-1]:
                t_end = t_start + t_window
                df_window = dl[(dl['time_h'] > t_start) & \
                                    (dl['time_h'] < t_end)]

                dil_rate = df_window['duration_s'].sum()/\
                (df_window['time_s'].iloc[-1] - df_window['time_s'].iloc[0])\
                * df_window['est_flow_rate_uL_per_s']                 

                df = pd.DataFrame()                                                 
                df['time_h'] = df_window['time_h']
                df['growth_rate_per_hr'] = dil_rate 

                gr = gr.append(df, ignore_index=True)                        

                t_start = df_window['time_h'].iloc[-1]

            gr_lst = []

            for i in range(0, len(POI_median[reactor]['time_norm'])):
                target = POI_median[reactor]['time_norm'].iloc[i]
                closest_value = abs(gr['time_h'] - target)
                idx = closest_value.idxmin()
                gr_lst.append(gr['growth_rate_per_hr'].iloc[idx])

            # Here I fix the gr at time 0, equal to 0.4
            target = 0
            closest_value = abs(POI_median[reactor]['time_norm'] - target)
            idx = closest_value.idxmin()

            gr_lst = (gr_lst/gr_lst[idx])*0.4

            POI_median[reactor]['gr'] = gr_lst

        #Save data in csv
        path_out = folder + '\\data_processed\\reactor-' + str(reactor) + '\\' 
        os.makedirs(path_out, exist_ok=True)

        LED[reactor].to_csv(path_out + 'LED_' + '.csv', index = False)    
        cells_raw[reactor].to_csv(path_out + 'cells_raw_' + '.csv', index = False) 
        cells_corrected[reactor].to_csv(path_out + 'cells_corrected_' + '.csv', index = False)
        cells[reactor].to_csv(path_out + 'cells_' + '.csv', index = False)
        POI[reactor].to_csv(path_out + 'POI_' + '.csv', index = False)
        POI_median[reactor].to_csv(path_out + 'POI_median_' + '.csv', index = False)
        
        