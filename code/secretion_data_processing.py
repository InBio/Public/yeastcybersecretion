def secretion_reference_batch(reference_batch, folder_source, folder_destiny):

    import pandas as pd
    import numpy as np
    import math
    import guava2data
    
    experiments_date = reference_batch
    
    print('Working on references')
    
    with open(folder_destiny + '\\Guava_compensation' + '.CSV', 'r') as infile:
        compensation_matrix = pd.read_csv(infile)

    for experiment in range(len(experiments_date)):

        print('Sample: ' + str(experiment + 1) + '/' + str(len(experiments_date)))

        experiment_date = experiments_date[experiment]
        path = folder_source + '\\' + str(experiment_date) + '_BEADS'
        sample_features =  pd.read_excel(path + '\\' + str(experiment_date) + '_Beads_summary.xlsx')
        data_sample_raw = list(range(len(sample_features)))
        size = len(sample_features)

        closest_data = []

        for date in compensation_matrix['Date_f2']:
            closest_data.append(abs(experiments_date[experiment] - date))

        a = closest_data.index(np.min(closest_data))
        correction_coef_FSC = 1/compensation_matrix.iloc[a][1]
        correction_coef_SSC = 1/compensation_matrix.iloc[a][2]
        correction_coef_GRN_V = 1/compensation_matrix.iloc[a][3]
        correction_coef_GRN_B = 1/compensation_matrix.iloc[a][4]
        correction_coef_ORG_G = 1/compensation_matrix.iloc[a][5]        

        for i in range(len(sample_features)):
            with open(path + '\\' + sample_features.iloc[i]['FSC_file_name']+ '\\' + \
                      sample_features.iloc[i]['FSC_file_name'] + '.' +  \
                      sample_features.iloc[i]['Well'] + '.CSV', 'r') as infile:
                data_sample_raw[i] = pd.read_csv(infile)

        # Gating the data
        # Here I am going to gate data by selecting only those events with ratio SSC/FSC > 10
        ## and FSC > 500 to not to select small beads

        selection_FSC_lower = 0
        selection_FSC_upper = np.inf
        selection_SSC_lower = 10**4
        selection_SSC_upper = np.inf

        data_sample_gated = list(range(len(sample_features)))
        data_sample_gated_filtered = list(range(len(sample_features)))

        data_sample_corrected = data_sample_raw.copy()

        for i in range(len(sample_features)):

            count_number = 0

            data_sample_corrected[i]['GRN-B-HLin'] = data_sample_raw[i]['GRN-B-HLin'] * correction_coef_GRN_B
            data_sample_corrected[i]['GRN-V-HLin'] = data_sample_raw[i]['GRN-V-HLin'] * correction_coef_GRN_V
            data_sample_corrected[i]['ORG-G-HLin'] = data_sample_raw[i]['ORG-G-HLin'] * correction_coef_ORG_G
            data_sample_corrected[i]['FSC-HLin'] = data_sample_raw[i]['FSC-HLin'] * correction_coef_FSC
            data_sample_corrected[i]['SSC-HLin'] = data_sample_raw[i]['SSC-HLin'] * correction_coef_SSC

            data_temp =[]

            for n in range(len(data_sample_corrected[i])):
                if data_sample_corrected[i].loc[n]['SSC-HLin']/data_sample_corrected[i].loc[n]['FSC-HLin']> 10:
                    if count_number == 0:
                        data_temp = pd.DataFrame(data_sample_corrected[i].loc[n])
                        data_temp = data_temp.transpose()
                        count_number = count_number + 1
                    else:
                        data_temp2 = pd.DataFrame(data_sample_corrected[i].loc[n])
                        data_temp2 = data_temp2.transpose()
                        data_temp = data_temp.append(data_temp2)

                else:
                    continue

            data_temp = data_temp[(data_temp['FSC-HLin']  > selection_FSC_lower) & (data_temp['FSC-HLin']  < selection_FSC_upper)]
            data_temp = data_temp[(data_temp['SSC-HLin']  > selection_SSC_lower) & (data_temp['SSC-HLin']  < selection_SSC_upper)]

            data_sample_gated[i] = data_temp
            data_temp = [data_temp]

        # Now take what is wanted for this analysis:

        index = sample_features.index

        condition = sample_features["Comments"] == "ref 1"
        ref_1 = index[condition]
            
        # Compute samples (I do not substract mCerulean contribution to the mNeon Channel because I do it in the main script)
        secretion = data_sample_gated[ref_1[0]]['GRN-B-HLin'].median()
        
        # Report on details        
        number_gated = len(data_sample_gated[ref_1[0]]['GRN-B-HLin'])
        total = len(data_sample_raw[ref_1[0]]['GRN-B-HLin'])
        filtered = number_gated
                
        events = filtered

        if filtered < 10:
            
            print('WARNING!!! number of events too low --> CHECK')
            print('-- -- -- -- -- -- -- -- -- -- -- -- --')
            print('Experiment date: ' + str(experiment_date))
            print('Total events: ' + str(total))
            print('Gated events: ' + str(number_gated))
            print('Filtered events: ' + str(filtered))
            print(' ')
            print('-- -- -- -- -- -- -- -- -- -- -- -- --')
            print(' ')
                
        data_df = pd.DataFrame()

        data_df['secretion'] = [secretion]
        data_df['events'] = [events]
        
        data_df.to_csv(folder_destiny + '\\' + str(experiments_date[experiment]) + '_reference_batch' + '.csv', index = False)
        
######################################################################################################################################################
######################################################################################################################################################
######################################################################################################################################################
######################################################################################################################################################
def secretion_processing(experiments_date, beads_date, reference_batch, folder_source, folder_destiny):

    import pandas as pd
    import numpy as np
    import math
    import guava2data

    print(' ')
    print('-- -- -- -- -- -- -- -- ')
    print('Working on samples')
    print('-- -- -- -- -- -- -- -- ')
    print(' ')

    with open(folder_destiny + '\\Guava_compensation' + '.CSV', 'r') as infile:
        compensation_matrix = pd.read_csv(infile)
        
    # Work on the reference batches            
    mNG_ref = []

    for reference_replica in range(len(reference_batch)):
        
        reference_batch_date = reference_batch[reference_replica]
        
        with open(folder_source + '\\' + str(reference_batch_date) + '_reference_batch' + '.csv', 'r') as infile:
            df = pd.read_csv(infile)
                    
        mNG_ref += [df['secretion'].values[0]]

    mNG_ref = np.mean(mNG_ref)        

    for experiment in range(len(experiments_date)):

        print('Sample: ' + str(experiment + 1) + '/' + str(len(beads_date)))

        experiment_date = experiments_date[experiment]
        bead_date = beads_date[experiment]
        path = folder_source + '\\' + str(experiment_date) + '\\' + str(bead_date) + '_BEADS'
        sample_features =  pd.read_excel(path + '\\' + str(bead_date) + '_Beads_summary.xlsx')
        data_sample_raw = list(range(len(sample_features)))
        size = len(sample_features)

        closest_data = []

        for date in compensation_matrix['Date_f2']:
            closest_data.append(abs(beads_date[experiment] - date))

        a = closest_data.index(np.min(closest_data))
        correction_coef_FSC = 1/compensation_matrix.iloc[a][1]
        correction_coef_SSC = 1/compensation_matrix.iloc[a][2]
        correction_coef_GRN_V = 1/compensation_matrix.iloc[a][3]
        correction_coef_GRN_B = 1/compensation_matrix.iloc[a][4]
        correction_coef_ORG_G = 1/compensation_matrix.iloc[a][5]        

        for i in list(sample_features[sample_features['Type'] == 'sample'].index):
            
            with open(path + '\\' + sample_features.iloc[i]['FSC_file_name']+ '\\' + \
                      sample_features.iloc[i]['FSC_file_name'] + '.' +  \
                      sample_features.iloc[i]['Well'] + '.CSV', 'r') as infile:
                data_sample_raw[i] = pd.read_csv(infile)

        # Gating the data
        # Here I am going to gate data by selecting only those events with ratio SSC/FSC > 10
        ## and FSC > 500 to not to select small beads

        selection_FSC_lower = 0 #500
        selection_FSC_upper = np.inf
        selection_SSC_lower = 10**4
        selection_SSC_upper = np.inf

        data_sample_gated = list(range(len(sample_features)))
        data_sample_gated_filtered = list(range(len(sample_features)))

        data_sample_corrected = data_sample_raw.copy()

        for i in list(sample_features[sample_features['Type'] == 'sample'].index):

            count_number = 0

            data_sample_corrected[i]['GRN-B-HLin'] = data_sample_raw[i]['GRN-B-HLin'] * correction_coef_GRN_B
            data_sample_corrected[i]['GRN-V-HLin'] = data_sample_raw[i]['GRN-V-HLin'] * correction_coef_GRN_V
            data_sample_corrected[i]['ORG-G-HLin'] = data_sample_raw[i]['ORG-G-HLin'] * correction_coef_ORG_G
            data_sample_corrected[i]['FSC-HLin'] = data_sample_raw[i]['FSC-HLin'] * correction_coef_FSC
            data_sample_corrected[i]['SSC-HLin'] = data_sample_raw[i]['SSC-HLin'] * correction_coef_SSC

            data_temp =[]

            for n in range(len(data_sample_corrected[i])):
                if data_sample_corrected[i].loc[n]['SSC-HLin']/data_sample_corrected[i].loc[n]['FSC-HLin']> 10:
                    if count_number == 0:
                        data_temp = pd.DataFrame(data_sample_corrected[i].loc[n])
                        data_temp = data_temp.transpose()
                        count_number = count_number + 1
                    else:
                        data_temp2 = pd.DataFrame(data_sample_corrected[i].loc[n])
                        data_temp2 = data_temp2.transpose()
                        data_temp = data_temp.append(data_temp2)

                else:
                    continue

            data_temp = data_temp[(data_temp['FSC-HLin']  > selection_FSC_lower) & (data_temp['FSC-HLin']  < selection_FSC_upper)]
            data_temp = data_temp[(data_temp['SSC-HLin']  > selection_SSC_lower) & (data_temp['SSC-HLin']  < selection_SSC_upper)]

            data_sample_gated[i] = data_temp
            data_temp = [data_temp]

        # Now take what is wanted for this analysis:
        secretion = []
        comments = list(range(1, len(list(sample_features[sample_features['Type'] == 'sample'].index)) + 1))
        events = []
        
        for i in list(sample_features[sample_features['Type'] == 'sample'].index):

            # Compute samples (I do not substract mCerulean contribution to the mNeon Channel because I do it in the main script)
            secretion += [data_sample_gated[i]['GRN-B-HLin'].median()]

            # Report on details        
            number_gated = len(data_sample_gated[i]['GRN-B-HLin'])
            total = len(data_sample_raw[i]['GRN-B-HLin'])
            filtered = number_gated
                
            events += [filtered]

            if filtered < 10:
            
                print('WARNING!!! number of events too low --> CHECK')
                print('-- -- -- -- -- -- -- -- -- -- -- -- --')
                print('Experiment date: ' + str(experiment_date))
                print('Total events: ' + str(total))
                print('Gated events: ' + str(number_gated))
                print('Filtered events: ' + str(filtered))
                print(' ')
                print('-- -- -- -- -- -- -- -- -- -- -- -- --')
                print(' ')
                
        data_df = pd.DataFrame()

        data_df['sample'] = comments
        data_df['secretion'] = secretion
        data_df['events'] = events

        data_df.to_csv(folder_destiny + '\\' + 'data_sec_' + str(beads_date[experiment]) + '.csv', index = False)
