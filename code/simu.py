# Simulation
import pandas as pd
import numpy as np
import scipy.integrate as integrate
 
def simulation(data_eval, induction_eval, parameters, model, start, end, y_0):

    # Simulation to reach steady state levels
    t_span_ss = [0, 100]
    I = 0
    args_ss = (parameters, I)

    result_ss = integrate.solve_ivp(model, t_span_ss, y_0, args = args_ss)
    
    # Simulation before induction
    y_ss0 = result_ss.y[:, -1].tolist()
    
    t_eval_dark = data_eval['time_norm'][(data_eval['time_norm']>start) & \
                                        (data_eval['time_norm']<0)]
    
    t_span_dark =  (t_eval_dark.iloc[0], t_eval_dark.iloc[-1])
    
    args_dark = (parameters, I)
    
    result_dark = integrate.solve_ivp(model, t_span_dark, y_ss0, t_eval = t_eval_dark, \
                                           args = args_dark)
    y_dark = result_dark.y.tolist()
    t_dark = result_dark.t.tolist()

    # Simulation after induction
    y_dark0 = result_dark.y[:, -1]
    
    t_eval_induction = data_eval['time_norm'][(data_eval['time_norm']>=0) & \
                                        (data_eval['time_norm']<end)]
    
    t_span_induction =  (t_eval_induction.iloc[0], t_eval_induction.iloc[-1])

    I = induction_eval
    
    args_induction = (parameters, I)
    
    result_induction = integrate.solve_ivp(model, t_span_induction, y_dark0, t_eval = t_eval_induction, \
                                           args = args_induction)
        
    t_induction = result_induction.t.tolist()
    t = t_dark + t_induction

    sim = pd.DataFrame()
    sim['time'] = t
    
    variables = {}

    for variable in range(0, len(y_0)):
        variable_induction = result_induction.y[variable, :].tolist()
        variables[variable] = y_dark[variable] + variable_induction
    
        sim['variable_' + str(variable)] = variables[variable]
        
    return sim